    NAME    GENERALMACROS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                GENERALMAC.INC                              ;
;                                General Macros                              ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; This file is used to show the expansions for macros.
;
; Revision History:
;       12/01/13   Sith Domrongkitchaiporn      Initial Version


CODE SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CODE, DS:NOTHING, ES:NOTHING

$INCLUDE(GenMac.inc)

MacroTable      Label   WORD

        DW      0CABh
        DW      0CAFEh
        DW      0FEEDh
        DW      0BEDh


        %CLR(AX)                        ;Clear register
        
        %SETBIT(AX,3)
        %SETBIT(AX,6)
        
        %CLRBIT(AX,3)

        %COMBIT(AX,1)

        %TESTBIT(AX,0)
        %TESTBIT(AX,6)

        %READPCB(100h)

        %WRITEPCB(100h,42)
        
        MOV     AX, 2
        LEA     BX, MacroTable
        %XLATW  ;MacroTable

        MOV     AX, 3
        LEA     BX, MacroTable
        %XLATW  ;MacroTable
        
CODE    ENDS

END        
