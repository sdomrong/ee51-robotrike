        NAME    MAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    MAIN                                    ;
;                            Homework #3 Test Code                           ;
;                                  EE/CS  51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Description:      This program tests the conversion functions for Homework
;                   #3.  If all tests pass it jumps to the label
;                   QueueGood.  If any test fails it jumps to the label
;                   QueueError.
;
; Input:            
; Output:           None
;
; User Interface:   No real user interface.  The user can set breakpoints at
;                   QueueGood and QueueError to see if the code is working
;                   or not.
;
; Error Handling:   If a test fails the program jumps to QueueError.
;
; Algorithms:       None
; Data Structures:  Queue in Memory
;
; Known Bugs:       None.
; Limitations:      Assumes correct passed in values
;
; Revision History:
;    11/02/13  Sith Domrongkitchaiporn	initial version   
;

$INCLUDE (Queue.inc)

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA, STACK

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP

;external function declarations
    EXTRN   QueueInit:NEAR
    EXTRN   QueueEmpty:NEAR
    EXTRN   QueueFull:NEAR
    EXTRN   Dequeue:NEAR
    EXTRN   Enqueue:NEAR
    EXTRN   QueueTest:NEAR

START:

MAIN:
        MOV     AX, DGROUP              ; initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(DGROUP:TopOfStack)
        MOV     AX, DGROUP              ; initialize the data segment
        MOV     DS, AX

        MOV     CX, 255                 ; size of queue
        LEA     SI, queue               ; move offset
        CALL    QueueTest               ; do the 
        JCXZ    QueueGood               ; go to appropriate infinite loop
        ;JMP    QueueError              ; based on return value

QueueError:                             ; test failed 
        JMP     QueueError              ; just sit here until get interrupted


QueueGood:                              ; all tests passed
        JMP     QueueGood               ; just sit here until get interrupted

CODE    ENDS


; Data
DATA    SEGMENT PUBLIC  'DATA'

queue           Q_STRUCT <>               ; Initializing queue 

DATA ENDS

        
; Stack
STACK   SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

END     START        
