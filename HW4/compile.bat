asm86 main.asm m1 db ep
asm86 display.asm m1 db ep
asm86 eventh.asm m1 db ep
asm86 segtab.asm m1 db ep
asm86 converts.asm m1 db ep

link86 main.obj, display.obj, eventh.obj, segtab.obj, converts.obj, hw4test.obj

loc86 main.lnk AD(SM(CODE(4000H), DATA(400H), STACK(7000H))) NOIC

pause