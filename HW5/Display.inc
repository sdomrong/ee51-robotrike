;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Display                                 ;
;                               Display Functions                            ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Revision History:
;    11/16/13  Sith Domrongkitchaiporn	initial version
;

SBUFFERLEN      EQU     8
SEG_SPACE       EQU     0
ASCII_NULL      EQU     0
DISPLAYPORT     EQU     00H

TIMEREOI        EQU     00008H

; Control Register Values
TMRCONVAL       EQU     00001H          ;set priority for timers to 1 and enable


; Timing Definitions
COUNTS_PER_MS   EQU     2304            ;number of timer counts per 1 ms
                                        ;(assumes 18.432 MHz clock)
FREQ            EQU     1000
T2CMPAVAL       EQU     COUNTS_PER_MS/FREQ * 1000  ;number of ms for each segment

; Chip Select Unit Definitions

; Addresses
PACSreg         EQU     0FFA4H          ;address of PACS register
MPCSreg         EQU     0FFA8H          ;address of MPCS register

; Control Register Values
PACSval         EQU     00003H          ;PCS base at 0, 3 wait states
                                        ;0000000000------  starts at address 0
                                        ;----------000---  reserved
                                        ;-------------0--  wait for RDY inputs
                                        ;--------------11  3 wait states
MPCSval         EQU     00183H          ;PCS in I/O space, use PCS5/6, 3 wait states
                                        ;0---------000---  reserved
                                        ;-0000001--------  MCS is 8KB
                                        ;--------1-------  output PCS5/PCS6
                                        ;---------0------  PCS in I/O space
                                        ;-------------0--  wait for RDY inputs
                                        ;--------------11  3 wait states

; Interrupt Vectors
Tmr2Vec         EQU     19               ;interrupt vector for Timer 2

; General Definitions

FIRST_RESERVED_VEC	EQU	1	;reserve vectors 1-3
LAST_RESERVED_VEC	EQU	3
NUM_IRQ_VECTORS         EQU     256     ;number of interrupt vectors

LEDDisplay      EQU     0080H           ;display address

NUM_DIGITS      EQU     8               ;number of digits in the display


