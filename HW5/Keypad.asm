NAME    Keypad

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Keypad                                  ;
;                               Keypad Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; KeypadScan:           Scan keypad for any buttons pressed
; KeypadInit:           Initializes the shared variables
;
; Revision History:
;       11/11/13   Sith Domrongkitchaiporn   Initial Revision
;       11/15/13   Sith Domrongkitchaiporn   Finish coding
;       11/16/13   Sith Domrongkitchaiporn   Fix Syntax - Code working
;       11/17/13   Sith Domrongkitchaipon    Update Comments

CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DATA

EXTRN   EnqueueEvent:NEAR

$INCLUDE (Keypad.inc)


; KeypadScan
;
; Description:          This function scans the keypad to see if any key is
;                       being pressed.  The keypad will start by scanning from
;                       the first row on the first collumn. It goes through each
;                       row until it sees a pressed button or until it scans all
;                       the keys. If two keys are being pressed at the same
;                       time, the function will take the one first scanned. 
;                       This function also will then debounce keypads.  Once 
;                       this is done, it will put the pressed button in a queue 
;                       to be executed.
;
; Operation:            The function scans through each of they key from 1st row
;                       and 1st collumn through each collumn in that row then 
;                       before moving to the next row.  The scanning continues
;                       until it sees a pressed button or it has scanned all the
;                       keys.  This function will then debounce keys.  This is 
;                       done by setting a counter and this counter is decrement
;                       for every subsequent time.  Once the counter gets to 0.
;                       It then throws an event.  This event is then put into a
;                       queue.
;
; Arguments:            Address of queue (SI)
;                       
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     queueadr, counter, read/write
;
; Global Variables:     None
; Input:                Keypad
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queue
;
; Limitations:          Can only scan in one key.
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/17/13

KeypadScan      PROC        NEAR
                PUBLIC      KeypadScan

KeypadScanInit:
        PUSH    DX                              ;Prevent Registers trashing
        PUSH    BX
        PUSH    AX
        MOV     DX, INITADR - 1                 ;Initialize which row
KeypadScanLoop:
        INC     DX                              ;Going to next row
        CMP     DX, INITADR + ROWS              ;If DX < No.rows,
        JB      KeypadRead                      ;Keep Scanning
        ;JAE    KeypadNone                      ;Otherwise, no keys pressed

KeypadNone:                                     ;If nothing is pressed,
        MOV     counter, DBTIME                 ;Reset counter to DBTIME
        JMP     KeypadScanDone                  ;We're done.

KeypadRead:
        IN      AL, DX                          ;Read in keypad value
        NOT     AL                              ;Keypad is active low
        ;JMP     KeypadFirst

KeypadFirst:
        TEST    AL, FIRSTCOLUMN                 ;If first key is pressed
        MOV     BL, 0                           ;for the first column
        JNZ     KeypadDBChk                     ;Go to keypadDebounce check
        ;JZ      KeypadSecond                    ;Otherwise, go check second key

KeypadSecond:
        TEST    AL, SECCOLUMN                   ;If second key is pressed
        MOV     BL, 1                           ;for the second column
        JNZ     KeypadDBChk                     ;Go to keypadDebounce check
        ;JZ      KeypadThird                     ;Otherwise, go check third key

KeypadThird:
        TEST    AL, THRCOLUMN                   ;If third key is pressed
        MOV     BL, 2                           ;for the third column
        JNZ     KeypadDBChk                     ;Go to keypadDebounce check
        ;JZ      KeypadFourth                   ;Otherwise, go check fourth key    

KeypadFourth:
        TEST    AL, FOURCOLUMN                  ;If fourth key is pressed
        MOV     BL, 3                           ;for the fourth column
        JNZ     KeypadDBChk                     ;Go to keypadDebounce
        JZ      KeypadScanLoop                  ;Otherwise, no key is pressed

KeypadDBChk:
        SUB     DX, INITADR                     ;Get which row the key is in
        SHL     DX, 2
        ADD     BL, DL 
        CMP     key, BL                         ;If the key pressed is the same,
        JE      KeypadDB                        ;Go to Debounce Key
        MOV     key, BL                         ;Store key pressed
        JMP     KeypadScanDone                  ;Otherwise, we're done                          

KeypadDB:
        DEC     counter                         ;Decrement counter
        CMP     counter, 0                      ;If counter has reached DBTIME
        JG      KeypadScanDone                  ;Not debounced, done
        ;JLE    KeypadEvent                     ;Enqueue Event

KeypadEvent:
        MOV     AH, 1                           ;Setting up for EnqueueEvent
        MOV     AL, key                         
        CALL    EnqueueEvent
        MOV     counter, REPEATRATE             ;Set up for repeat rate
        JMP     KeypadScanDone                  ;We're done.
                         
KeypadScanDone:
        POP     AX                              ;Put back old register values
        POP     BX
        POP     DX
        RET     

KeypadScan      ENDP


; KeypadInit
;
; Description:          This function initializes the keypad by setting the 
;                       counter = 0.  It will initialize the queue by calling 
;                       QueueInit.  This function also sets the address of queue
;                               
; Operation:            The functions intializes the value of counter to 0.  It
;                       sets the shared variable queueadr to the right queue
;                       address.  This function calls QueueInit to set up the
;                       queue for use. 
;                         
;
; Arguments:            Address of queue (SI)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     counter, queueadr
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:		None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/17/13


KeypadInit      PROC        NEAR
                PUBLIC      KeypadInit

KeypadInitInit:
        MOV     COUNTER, DBTIME                 ;Initialize Counter
        MOV     key, -1                         ;Initialize Key
        RET

KeypadInit     ENDP


CODE    ENDS


; Data
DATA    SEGMENT PUBLIC  'DATA'

counter         DW      ?                       ;Counter for Debounce Time
key             DB      ?                       ;Key pressed    

DATA ENDS

END
