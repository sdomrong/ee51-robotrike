;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Display                                 ;
;                               Display Functions                            ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Revision History:
;    11/15/13  Sith Domrongkitchaiporn	initial version
;    11/17/13  Sith Domrongkitchaiporn  Change Debounce Time   
;

INITROW         EQU     -1              ;Initial Row   
INITADR         EQU     80H             ;Initial Keypad Address
ROWS            EQU     4               ;Number of rows in Keypad
COUNTERTIME     EQU     0               ;The key has reached debounced time
DBTIME          EQU     25              ;Debounce time
KEYPAD          EQU     1               ;Event Sent
REPEATRATE      EQU     500             ;Auto-repeat rate


;Scanning Each Column
FIRSTCOLUMN     EQU     00000001B       ;First column pressed
SECCOLUMN       EQU     00000010B       ;Second column pressed
THRCOLUMN       EQU     00000100B       ;Third column pressed
FOURCOLUMN      EQU     00001000B       ;Fourth column pressed

