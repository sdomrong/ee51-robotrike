;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  GENMAC.INC                                ;
;                                General Macros                              ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Contains a few macros for motor usage
;
; Revision History:
;       11/21/13    Sith Domrongkitchaiporn    Initial Version
;
;
; XLATW
;
; Description:       This macro does the XLAT function for words
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: AX, BX
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/31/13

%*DEFINE(XLATW)   (
        PUSH    SI                              ;Prevent Register Trashing
        SHL     AX, 1                           ;Since this is for words, we
                                                ;multiply by 2
        ADD     AX, BX                          ;Add offset to actual value
        MOV     SI, AX                  
        MOV     AX, CS:[SI]
        POP     SI
)

$LIST
