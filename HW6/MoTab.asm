        NAME  MOTAB

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   MOTORTABLE                               ;
;                              Tables of motor values                        ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; This file contains a table of force vectors for the three motors used by the
; RoboTrike and the motor bits for the parralel port.
;
; Revision History:
;       11/21/13   Sith Domrongkitchaiporn   Initial Version
;

CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'

; ForceTable
;
; Description:      This is the motor force table for the motor functions.
;                   It contains the value of force needed for the speed of each
;                   motor
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/21/13
;

ForceTable      LABEL   WORD
                PUBLIC  ForceTable

        DW      7FFFh           ; Motor1 Fx
        DW      0000h           ; Motor1 Fy
        DW      0C000h          ; Motor2 Fx
        DW      9127h           ; Motor2 Fy
        DW      0C000h          ; Motor3 Fx
        DW      6ED9h           ; Motor3 Fy


; MotorOnTable
;
; Description:      This is the motor BITs table for turning each motor on.
;                   It contains the value of bits needed to turn motor on.
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/21/13
;

MotorOnTable    LABEL   BYTE
                PUBLIC  MotorOnTable

        DB      00000010B       ; Turn Motor 1 on
        DB      00001000B       ; Turn Motor 2 on
        DB      00100000B       ; Turn Motor 3 on



; MotorDirTable
;
; Description:      This is the motor BITs table for each motor direction.
;                   It contains the value of bits for motor turning direction.
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/21/13
;

MotorDirTable   LABEL   BYTE
                PUBLIC  MotorDirTable

        DB      00000001B       ; Turn Motor 1 backwards
        DB      00000100B       ; Turn Motor 2 backwards
        DB      00010000B       ; Turn Motor 3 backwards

CODE    ENDS

        END