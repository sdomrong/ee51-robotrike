NAME    Motor

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Motor                                   ;
;                               Motor Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Table of Contents
;
;
;
; Revision History
;       11/21/13   Sith Domrongkitchaiporn   Initial Revision
;       11/23/13   Sith Domrongkitchaiporn   Finished coding
;       11/24/13   Sith Domrongkitchaiporn   Rewrote PWM (works now)

CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DATA
        
EXTRN   ForceTable:WORD
EXTRN   MotorOnTable:BYTE
EXTRN   MotorDirTable:BYTE
EXTRN   Sin_Table:WORD
EXTRN   Cos_Table:WORD

$INCLUDE (Motor.inc)
$INCLUDE (GenMac.inc)        

; InitMotor
;
; Description:          This function initializes the shared variable needed for
;                       the motors.
;
; Operation:            This function initializes the shared variables for the 
;                       motors.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     speed
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:		None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

InitMotor       PROC        NEAR
                PUBLIC      InitMotor

InitMotorInit:
        PUSH    AX                              ;Save old registers
        PUSH    DX
        PUSH    SI
        MOV     SI, 0                           ;Clear SI for use as counter
        MOV     speed, 0                        ;initialize speed to 0
        MOV     angle, 0                        ;initialize angle to 0
        MOV     counter, 0                      ;initialize counter to 0
        MOV     laser, 0                        ;initialize laser to be off
        MOV     AX, PCTRL                       ;Prepare to output to 
        MOV     DX, 183h                        ;parallel I/O
        OUT     DX, AX         
        
InitMotorLoop:        
        CMP     SI, NUMMOTOR                    ;If counter is < no.motor
        JLE     InitMotorArray                  ;Continue with loop
        JA      InitMotorDone                   ;Otherwise, we're done.
        
InitMotorArray:
        MOV     motor[SI], 0                    ;Clear value in motor array
        INC     SI
        JMP     InitMotorLoop

InitMotorDone:
        POP     SI                              ;Return old register values
        POP     DX
        POP     AX
        RET

InitMotor   ENDP

; SetMotorSpeed
;
; Description:          This function is passed two arguments, speed and angle.
;                       The speed is passed in via AX and is the absolute speed
;                       for which the RoboTrike is to run.  If speed input is
;                       IGNORESPEED, the speed argument is ignored.  The angle 
;                       is passed in via BX and is the signed angle for the 
;                       RoboTrike is to move.  0 degrees is straight, relative
;                       to its orientation.  If the angle input  is IGNOREANGLE,
;                       the angle argument is ignored.
;
; Operation:            This function takes the speed and angle arguments and
;                       puts it through a formula to calculate the speed and for
;                       which each wheel is supposed to turn.  This formula is
;                       described in the algorithm section.  The speed is stored
;                       in a shared variable.  The value of motors is stored in
;                       an array called motor.  Motor array stores the direction
;                       the motor should turn (forward/backward).
;
; Arguments:            Speed (AX)
;                       Angle (BX)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     speed, angle, arrayS, arrayD
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           Formula for each wheel
;                       S = Fx * v * cos d + Fy * v * sin d
;
; Data Structures:      None
;
; Limitations:          Assumes that values passed in are valid.  65534 is the 
;                       max speed.  
;
; Known Bugs:		None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

SetMotorSpeed   PROC        NEAR
                PUBLIC      SetMotorSpeed

        PUSHA                                   ;Save registers
                
MotorSpeedChk:
        CMP     AX, IGNORESPEED                 ;If speed passed != IGNORESPEED
        JNE     MotorSpeedAdjust                ;Go make room for signed bit     
        MOV     AX, speed                       ;Otherwise, we use the old speed
        JMP     MotorAngleChk                   

MotorSpeedAdjust:        
        SHR     AX, 1                           ;Make room for signed bit
        
MotorAngleChk:
        CMP     BX, IGNOREANGLE                 ;If angle passed != IGNOREANGLE
        JNE     SetMotorStore                   ;We're done with checking
        MOV     BX, angle                       ;Otherwise, we use the old angle
        ;JMP    SetMotorStore

SetMotorStore:  
        MOV     speed, AX                       ;Put new speed in speed
        MOV     angle, BX                       ;Put new angle in angle

SetMotorModAngle:
        MOV     AX, angle                       ;Prepare for IDIV
        MOV     BX, ROBOANGLE                   ;Put angle into register
        CWD
        IDIV    BX                              ;Signed division for remainder
        OR      DX, DX                          ;Check sign flag
        JNS     SetMotorAngleDone               ;If not signed, do nothing
        ADD     DX, ROBOANGLE                   ;Otherwise, do ROBOANGLE + angle
        ;JMP     SetMotorAngleDone

SetMotorAngleDone:
        MOV     angle, DX                       ;Now we have angle range from
                                                ; 0 - ROBOANGLE degress

SetMotorLoopInit:
        MOV     DI, 0                           ;Initialize DI as force counter
        MOV     SI, 0                           ;Initialize Motor counter 

SetMotorLoopChk:
        CMP     SI, NUMMOTOR                    ;If SI < Number of Motors
        ;JB     SetMotorLoop                    ;keep looping
        JAE     SetMotorLoopDone                ;otherwise, we're done.

SetMotorCosLoop:  
        LEA     BX, ForceTable                  ;Get address of force table
        MOV     AX, DI                          ;put counter value in AX  (XLAT)             
        %XLATW   ;ForceTable                      ;Look Up Force value from table
        IMUL    speed                           ;Multiply Force by speed
        MOV     AX, WORD PTR angle              ;Move angle into AX for XLAT
        LEA     BX, Cos_Table                   ;Get address of cos table
        %XLATW   ;Cos_Table                       ;Look up cos value from table    
        IMUL    DX                              ; = force*speed*cos
        SHL     DX, 2                           ;Get rid of 2 signed bits from
                                                ;multiplication
        MOV     CX, DX                          ;Save Xdirection motor value 
        INC     DI                              ;counter++ for next force value
        JMP     SetMotorSinLoop

SetMotorSinLoop:
        LEA     BX, ForceTable                  ;Get address of force table
        MOV     AX, DI                          ;put counter value in AX  (XLAT)             
        %XLATW   ;ForceTable                      ;Look Up Force value from table
        IMUL    speed                           ;Multiply Force by speed
        MOV     AX, WORD PTR angle                       ;Move angle into AX for XLAT
        LEA     BX, Sin_Table                   ;Get address of sin table
        %XLATW   ;Sin_Table                       ;Look up sin value from table    
        IMUL    DX                              ; = force*speed*sin
        SHL     DX, 2                           ;Get rid of 2 signed bits from
                                                ;multiplication 
        ADD     DX, CX                          ;motor = Fx + Fy
        MOV     motor[SI], DH                   ;Store value for motor in array
        INC     DI
        INC     SI                              ;Go to next motor
        JMP     SetMotorLoopChk                 ;Loop back to check conditions

SetMotorLoopDone:
        POPA
        RET

SetMotorSpeed   ENDP


; GetMotorSpeed
;
; Description:          This function returns the current speed for the Robo-
;                       Trike.  A speed of 65534 indicates the maximum speed and
;                       a value of 0 indicates the RoboTrike is stopped. The 
;                       value returned will always be between 0 and 65534
;                       inclusively.
;
; Operation:            This function looks up the speed shared variable and
;                       returns it.
;
; Arguments:            None
;
; Return Value:         Speed (AX)
; Local Variables:      None
; Shared Variables:     speed
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:		None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

GetMotorSpeed   PROC        NEAR
                PUBLIC      GetMotorSpeed

        MOV     AX, speed
        RET

GetMotorSpeed   ENDP


; GetMotorDirection
;
; Description:          This function returns the current direction the Robo-
;                       Trike is heading in relative to the RoboTrike's orien-
;                       tation.  Angles are measured clockwise.  The value will
;                       be from 0-359.
;
; Operation:            This function looks up the angle shared variable and
;                       returns it.  If the angled is negative,  we need to 
;                       subtract that from 360 to ensure that we get a returned
;                       value of 0-359.
;
; Arguments:            None
;
; Return Value:         Angle (AX)
; Local Variables:      None
; Shared Variables:     speed
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None  
;
; Known Bugs:		None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

GetMotorDirection PROC        NEAR
                  PUBLIC      GetMotorDirection

        MOV     AX, angle
        RET

GetMotorDirection       ENDP


; SetLaser
;
; Description:          The function is passed a single argument (onoff) in AX 
;                       that indicates whether to turn the RoboTrike laser on or
;                       off. A zero (0) value turns the laser off and a non-zero
;                       value turns it on.
;
; Operation:            This function turns on the laser if the argument passed
;                       is non-zero.  If 0, the laser is turned off.  The
;                       function also stores whether the laser is on or off in a
;                       shared variable.
;
; Arguments:            Laser Value (AX)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     laser
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes that values passed in are valid.
;
; Known Bugs:		None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13


SetLaser        PROC        NEAR
                PUBLIC      SetLaser

SetLaserLoop:
        CMP     AX, LASEROFF
        ;JE      SetLaserOff
        JNE     SetLaserOn

SetLaserOff:
        MOV     laser, LASEROFF
        JMP     SetLaserDone

SetLaserOn:
        MOV     laser, LASERON
        ;JMP     SetLaserDone

SetLaserDone:
        RET

SetLaser        ENDP


; GetLaser
;
; Description:          The function returns whether the laser is on or off
;
; Operation:            This function returns 1 if the laser is on and 0 if the 
;                       laser is off.  The return status is in AX.
;
; Arguments:            None
;
; Return Value:         laser on/off (AX)
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:		None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

GetLaser        PROC        NEAR
                PUBLIC      GetLaser

        MOV     AL, laser 
        RET

GetLaser        ENDP

; PWM
;
; Description:          The function turns on the motor by receiving a call from
;                       the timer event handler. This function also turns on the
;                       laser.
;
; Operation:            This function turns the motor on if motor > counter.
;                       The direction that the motor turns (forward/backwards)
;                       depends on signed bit.  The laser is also output if
;                       laser = 1.  Otherwise, it is turned off.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     counter, motor, laser
;
; Global Variables:     None
; Input:                None
; Output:               Motors
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/18/13

PWM             PROC        NEAR
                PUBLIC      PWM

        PUSH    AX
        PUSH    BX
        PUSH    CX
        PUSH    DX
        PUSH    SI
        
PWMINIT:
        MOV     SI, 0                           ;initialize motor count
        MOV     CX, 0                           ;Clear CX (Used for output)
        MOV     AX, 0                           

PWMLaser:
        CMP     laser, LASEROFF                 ;if laser == off
        JE      PWMMotorChk                     ;do nothing
        OR      CL, LASERBIT                    ;otherwise, turn laser on
        ;JMP     PWMMotorChk
        
PWMMotorChk:
        CMP     SI, NUMMOTOR                    ;if motor counter < no.motors
        JB      PWMDir                          ;go find the direction
        JAE     PWMOutput                       ;otherwise, output our results

PWMDir:
        MOV     AL, Motor[SI]                   ;Move motor value into register
        OR      AL, AL                          ;Check if the number is -ve
        JS      PWMBackward                     ;yes -> go turn on backward bit
        JNS     PWMCounterLoop                  ;no -> skip turning on bit  

PWMBackward:
        MOV     AX, SI                          ;Move motor number into AX
        LEA     BX, MotorDirTable               ;Prepare for XLAT
        XLAT    MotorDirTable
        OR      CL, AL                          ;put bits in output register
        JMP     PWMCounterLoop       
        
PWMCounterLoop:
        CMP     counter, 128                    ;if counter < MAXCOUNT,
        JB      PWMABSMotor                     ;Carry on
        MOV     counter, 0                      ;Otherwise, reset the counter.
        ;JMP     PWMMotorChk     
        
PWMABSMotor:
        MOV     AL, Motor[SI]                   ;Put motor value into AL
        OR      AL, AL                          ;If value != -ve
        JNS     PWMCounterChk                   ;we're fine
        NEG     AL                              ;otherwise, take the absolute
                                                ;value

PWMCounterChk:
        CMP     AL, counter                     ;If motor value > counter
        JA      PWMOn                           ;turn motor on
        INC     SI                              ;otherwise, increase motor count
        JMP     PWMMotorChk                     ;and check the next motor

PWMOn:                                          ;if motor is on
        MOV     AX, SI                          ;Prepare to turn that motor on
        LEA     BX, MotorOnTable                ;
        XLAT    MotorOnTable
        OR      CL, AL                          ;Put bits in output register
        INC     SI                              ;incrase motor count
        JMP     PWMMotorChk                     ;check for next motor

PWMOutput:
        MOV     AL, CL                          ;put output values into AL
        MOV     DX, 181H                        ;set output port
        OUT     DX, AL          
        INC     counter

        POP     SI
        POP     DX
        POP     CX
        POP     BX
        POP     AX
        
        RET

PWM     ENDP


SetRelTurretAngle PROC        NEAR
                  PUBLIC      SetRelTurretAngle
 
        RET

SetRelTurretAngle ENDP

SetTurretAngle PROC        NEAR
               PUBLIC      SetTurretAngle
 
        RET

SetTurretAngle ENDP

GetTurretAngle PROC        NEAR
               PUBLIC      GetTurretAngle
 
        RET

GetTurretAngle ENDP




CODE    ENDS

; Data
DATA    SEGMENT PUBLIC  'DATA'

counter         DB    ?                         ;Motor time count
laser           DB    ?                         ;Laser on/off 
speed           DW    ?                         ;Speed of RoboTrike
angle           DW    ?                         ;Direcion of RoboTrike
motor           DB NUMMOTOR DUP (?)             ;Array for each motor counts

DATA ENDS

END