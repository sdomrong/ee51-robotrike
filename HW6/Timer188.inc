;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                 TIMER188.INC                               ;
;                           80188 Timer Definitions                          ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the definitions for the 80188 Timer Controller
;
;
; Revision History
;       11/05/13   Sith Domrongkitchaiporn  Initial Version
;      

; Address
;PeriphBase      EQU     0FF00H               ;Base address of all peripherals

; OFFSET
; Timer Control Register
TMRCON          EQU     PeriphBase + 32H        ; TCUCON Register

; Timer 0
T0CNTADR        EQU     PeriphBase + 50H        ; T0CNT Register
T0CMPAADR       EQU     PeriphBase + 52H        ; T0CMPA Register
T0CMPBADR       EQU     PeriphBase + 54H        ; T0CMPB Register
T0CONADR        EQU     PeriphBase + 56H        ; T0CON Register

; Timer 1
T1CNTADR        EQU     PeriphBase + 58H        ; T1CNT Register
T1CMPAADR       EQU     PeriphBase + 5AH        ; T1CMPA Register
T1CMPB          EQU     PeriphBase + 5CH        ; T1CMPB Register
T1CONADR        EQU     PeriphBase + 5EH        ; T1CON Register

; Timer 2
T2CNTADR        EQU     PeriphBase + 60H        ; T2CNT Register
T2CMPAADR       EQU     PeriphBase + 62H        ; T2CMPA Register
T2CONADR        EQU     PeriphBase + 66H        ; T2CON Register



; Register Definittion

; Timer Control Registers 0 and 1
TMRCON1EN       EQU     1000000000000000B         ; Enable Timer
TMRCON1DIS      EQU     0000000000000000B         ; Disable Timer
TMRCON1INH      EQU     0100000000000000B         ; Enable write to EN
                                                  ; INH is not stored
TMRCON1INHCLR   EQU     0000000000000000B         ; Ignore write to EN
TMRCON1INT      EQU     0010000000000000B         ; Generate Interrupt Request
                                                  ; when Max count is reached
TMRCON1INTCLR   EQU     0000000000000000B         ; Disable Interrupt Request
TMRCON1RIUB     EQU     0001000000000000B         ; Count register = Compare B
TMRCON1RIUA     EQU     0000000000000000B         ; Count register = Campare A
TMRCON1MC       EQU     0000000000100000B         ; Set when count reaches Max
                                                  ; Must be cleared
TMRCON1MCCLR    EQU     0000000000000000B         ; Count not at Max
TMRCON1RTG      EQU     0000000000010000B         ; Reset Count
TMRCON1RTGCLR   EQU     0000000000000000B         ; Enable Count
TMRCON1P        EQU     0000000000001000B         ; Increment timer when TMR2
                                                  ; reach max count
TMRCON1PCLR     EQU     0000000000000000B         ; increment timer at 0.25
                                                  ; CLKOUT
TMRCON1EXT      EQU     0000000000000100B         ; Use external clock
TMRCON1INTR     EQU     0000000000000000B         ; Use internal clock
TMRCON1DUAL     EQU     0000000000000010B         ; Dual max count mode
TMRCON1SING     EQU     0000000000000000B         ; Single max count mode
TMRCON1CONT     EQU     0000000000000001B         ; Timer run continously
TMRCON1STOP     EQU     0000000000000000B         ; Disable after count sequence

; Timer Control Register 2
TMRCON2EN       EQU     1000000000000000B         ; Enable Timer
TMRCON2DIS      EQU     0000000000000000B         ; Disable Timer
TMRCON2INH      EQU     0100000000000000B         ; Enable write to EN
                                                  ; INH is not stored
TMRCON2INHCLR   EQU     0000000000000000B         ; Ignore write to EN
TMRCON2INT      EQU     0010000000000000B         ; Generate Interrupt Request
                                                  ; when Max count is reached
TMRCON2INTCLR   EQU     0000000000000000B         ; Disable Interrupt Request
TMRCON2MC       EQU     0000000000100000B         ; Set when count reaches Max
                                                  ; Must be cleared
TMRCON2MCCLR    EQU     0000000000000000B         ; Count not at Max
TMRCON2CONT     EQU     0000000000000001B         ; Timer run continously
TMRCON2STOP     EQU     0000000000000000B         ; Disable after count sequence

; Timer Count Register
TMRCNT          EQU     1111111111111111B         ; Current timer count

; Timer Maxcount Compare Register
TMRCMPA         EQU     1111111111111111B         ; Max value for timer before
                                                  ; reset

