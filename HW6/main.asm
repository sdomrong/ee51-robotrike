        NAME    MAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    MAIN                                    ;
;                            Homework #5 Test Code                           ;
;                                  EE/CS  51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Description:  This program tests the display function of hw5.  The main
;               loop initializes the display, keypad, the event handler and 
;               calls the test code.       
;
; Revision History:
;    11/02/13  Sith Domrongkitchaiporn	initial version

CGROUP  GROUP   CODE
DGROUP  GROUP   STACK

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP

;external function declarations
        EXTRN   InitMotor:NEAR
        EXTRN   InitCS:NEAR
        EXTRN   ClrIRQVectors:NEAR
        EXTRN   InstallHandler:NEAR
        EXTRN   InitTimer:NEAR
        EXTRN   MotorTest:NEAR
     

START:

MAIN:
        CLI                             ;Clear any initial interrupts
        MOV     AX, STACK               ;initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(TopOfStack)
        
        MOV     AX, DGROUP              ; initialize the data segment
        MOV     DS, AX

        CALL    InitCS                  ;initialize the 80188 chip selects
                                        ;   assumes LCS and UCS already setup

        CALL    ClrIRQVectors           ;clear (initialize) interrupt vector table

        CALL    InstallHandler          ;install the event handler
                                        ;   ALWAYS install handlers before
                                        ;   allowing the hardware to interrupt.

        CALL    InitTimer               ;initialize the internal timer
        
        CALL    InitMotor

        STI                             ;and finally allow interrupts.
        
        CALL    MotorTest               ;Call test code

Forever:
        JMP     Forever                 ;sit in an infinite loop, nothing to
                                        ;   do in the background routine
        HLT                             ;never executed (hopefully)



CODE    ENDS


; Stack
STACK   SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

END     START   
