;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                GENERALMAC.INC                              ;
;                                General Macros                              ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Contains a few macros for general usage
;
; Revision History:
;       11/21/13    Sith Domrongkitchaiporn    Initial Version
;       11/27/13    Sith Domrongkitchaiporn    Finished Extra Credit 
;
; CLR(reg)
;
; Description:       This macro clears the register.
;
; Operation:         This macro sets a register to 0
;
; Arguments:         None
;
; Registers Changed: reg
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(CLR(reg))(
        XOR     %reg, %reg
)


; SETBIT(reg, bit)
;
; Description:       This macro set nth bit in register.  The first bit is the 
;                    0th bit.
;
; Operation:         This macro generates a mask which is used to OR with the 
;                    passed in register.
;
; Arguments:         None
;
; Registers Changed: reg
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(SETBIT(reg, bit))(
        PUSH    BX
        MOV     BX, 1
        SHL     BX, %bit
        OR      %reg, BX     
        POP     BX
)

; CLRBIT(reg, bit)
;
; Description:       This macro clears nth bit in register. The first bit is the 
;                    0th bit.
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: reg
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(CLRBIT(reg, bit))(
        PUSH    BX
        MOV     BX, 1
        SHL     BX, %bit
        NOT     BX
        AND     %reg, BX     
        POP     BX
)

; COMBIT(reg, bit)
;
; Description:       This macro invert the nth bit in register.  The first bit  
;                    is the 0th bit.
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: reg
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(COMBIT(reg, bit))(
        PUSH    BX
        MOV     BX, 1
        SHL     BX, %bit
        XOR     %reg, BX     
        POP     BX
)

; TESTBIT(reg, bit)
;
; Description:       This macro sets the zero flag if the nth bit is 0 and it
;                    resets the zero flag if the nth bit is 1.
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: reg
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(TESTBIT(reg, bit))(
        PUSH    BX
        MOV     BX, 1
        SHL     BX, %bit
        TEST    %reg, BX
        POP     BX
)


; XLATW
;
; Description:       This macro does the XLAT function for words
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: AX, BX
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(XLATW)   (
        PUSH    SI                              ;Prevent Register Trashing
        SHL     AX, 1                           ;Since this is for words, we
                                                ;multiply by 2
        ADD     AX, BX                          ;Add offset to actual value
        MOV     SI, AX                  
        MOV     AX, CS:[SI]
        POP     SI
)

; READPCB(addr)
;
; Description:       This macro reads the register at the passed in address and
;                    return it via AX
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: AX
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(READPCB(addr))(
        PUSH    DX
        MOV     DX, %addr
        IN      AL, DX 
        POP     DX
)


; WRITEPCB(addr, val)
;
; Description:       This macro writes to the register at the passed in address 
;                    and returns the value of register via AX
;
; Operation:         
;
; Arguments:         None
;
; Registers Changed: None
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/27/13

%*DEFINE(WRITEPCB(addr, val))(
        PUSH    DX
        MOV     DX, %addr
        MOV     AL, %val
        OUT     DX, AL
        POP     DX  
)







$LIST
