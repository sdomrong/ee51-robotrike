NAME    QUEUE

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Queue                                   ;
;                               Queue Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; file description including table of contents
;
; Revision History:
;       10/30/13   Sith Domrongkitchaiporn   Initial Revision
;       11/02/13   Sith Domrongkitchaiporn   Fix syntax  
;       12/01/13   Sith Domrongkitchaiporn   Update QueueInit

CGROUP  GROUP   CODE


CODE	SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP

$INCLUDE (Queue.inc)

; QueueInit
;
; Description:          Initialize the queue of the length (l) and element size 
;                       (s) at the passed address (a) and prepare the queue for 
;                       use.  First, we allocate 257 bytes array of memory.  The
;                       length (l) will be ignored and we will chose 256 for 
;                       byte queues and 128 for word queues.  The size (s) is 
;                       used to determine whether the entry is a byte or a word.
;                       The queue will be stored in memory starting at address 
;                       (a).  It will have a circular architecture, meaning that
;                       once the queue runs out of length, it will start storing
;                       at the beginning.  The queue is considered full when 
;                       there is only one space left in the queue.  Head and
;                       tail pointers are set to 0 since the queue is empty.
;
; Operation:            The function is passed in a length (l) as the length of 
;                       the queue.  This length will be 128 for word queus and 
;                       256 for byte queues.  The size (s) is passed in as true 
;                       or false.  If false (0), the elements are bytes.  If 
;                       true (!=0), the elements are words. The length (l) is 
;                       passed by AX, the size (s) is passed by BL and the 
;                       address (a) is passed by SI and the queue starts DS:SI.
;                       Since the queue is empty, the head and tail pointers = 0
;
; Arguments:            Length (AX), Size (BL), and Address (SI)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     head, tail, length, size, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           Array length: 2^n > length + 1. Until this is satisfied,
;                       we will multiplying our length by 2.
;
; Data Structures:      Assumes pointers passed are valid.  Size of queue is 
;                       power of 2.  Queue cannot be more than 256 bytes.
;
; Limitations:          Assumes pointer passed is valid.
;
; Known Bugs:		    None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        10/31/13
;
; Pseudo Code
; QueueInit(a, l, s)
;
; pass_length;                           //Passed in via AX
; pass_size;                             //Passed in via BL
; pass_address;                          //Passed in via SI
; int length = 1;                        //Initialize lenght of queue
;
; while (length <= pass_length){         //Try to get the queue length as powers
;        length = length * 2;            //of 2
; }
;
; head = 0;                              //Initialize where head is pointing
; tail = 0;                              //Initialize where tail is pointing
;
; //For the ease of moving the head and tails along the queue
;
; if (size == 0) {                       //If it's a byte
;        size = 1;                       //the size is 1
; }
; else {                                 //if it's a word
;        size = 2;                       //the size is 2
; }
; 
; //We only allocated 256 bytes in total.  The length cannot be more.
;
; length = min(length, MAX_QLENGTH >> size-1);
;
; queue.head = head;                       //Put information in memory
; queue.tail = tail;
; queue.length = length;
; queue.size = size;


QueueInit       PROC        NEAR
                PUBLIC      QueueInit

LengthInit:
        MOV     [SI].head, INIT_PTR     ; initialize head pointer to start
        MOV     [SI].tail, INIT_PTR     ; initialize tail pointer to start
        MOV     [SI].len, 255           ; 
        ;JMP    SizeCheck

SizeCheck:                              ; Check if the queue is for words
        CMP     BL, 0                   ; or bytes     
        ;JZ      ByteQueue               ; If 0, the queue is for bytes
        JNZ     WordQueue               ; If =!0, the queue is for words

ByteQueue:                              ; If we have bytes
        MOV     [SI].siz, BYTE_SIZE     ; Set the size of element to byte     
        MOV     [SI].len, MAX_BYTE_QUEUE; Make queue length = max of byte queues
        JMP     EndCheckInit            ; We're done adjusting length

WordQueue:                              ; If we have words
        MOV     [SI].siz, WORD_SIZE     ; Set the size of element to word
        MOV     [SI].len, MAX_WORD_QUEUE; Make queue length = max of word queues
        ;JMP     EndCheckInit           ; We're done adjusting length        

EndCheckInit:
        RET

QueueInit	ENDP




; QueueEmpty
;
; Description:          This function checks whether the queue at given address
;                       (a) is empty.  It checks by looking at whether the head 
;                       pointer is the same as the tail pointer or not.  If yes,
;                       zero flag is set.  If not, the zero flag is reset.
;
; Operation:            The function checks whether the queue at address (a) is 
;                       empty or not.  It looks up the position of head and tail
;                       pointers from memory and see if they are the same.  If 
;                       yes, zero flag is set.  If not, the zero flag is reset.
;                       The address (a) is passed by SI.
;
; Arguments:            Address (SI)
;
; Return Value:         Zero Flag
; Global Variables:     None
; Shared variables:     head, tail
;
; Local variables:      None
; Inputs:               Queue in Memory
; Outputs:              None
;
; Error Handling:       None
; Algorithms:           None
; Data Structures:      Queue
;
; Limitations:          Assumes pointer passed are valid
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/01/13
;
; Pseudo Code
; QueueEmpty(a)
;
; pass_address;                           //Passed by SI
;
; if (queue.head == queue.tail) {         //If position of head = position of tail
;        return ZeroFlag = 1;             //Set Zero Flag
; }
; else {
;        return ZeroFlag = 0;             //Reset Zero Flag
; }
 

QueueEmpty      PROC        NEAR
                PUBLIC      QueueEmpty

CheckEmpty:
        Push    BX                      ; Prevent register trashing
        MOV     BX, [SI].head           ; Cannot CMP memory with memory
        CMP     BX, [SI].tail           ; Check if head == tail
        POP     BX                      ; Get back old register value
        RET                             ; If yes, zero flag is set and return      

QueueEmpty      ENDP




; QueueFull
;
; Description:          This function checks whether the queue at given address 
;                       (a) is full or not.  It checks by looking at whether the
;                       tail pointer+1 cycles back to equal head pointer or not.
;                       If yes, zero flag is set.  If not, the zero flag is 
;                       reset.
;
; Operation:            The function checks whether the queue at address (a) is 
;                       full or not.  Due to the circular architecture, the 
;                       condition that we are checking is: 
;                                   head == (tail + 1) mod length
;                       If yes, zero flag is set.  If not, zero flag is reset.  
;                       The address (a) is passed by SI.
;
; Arguments:            Address (SI)
;
; Return Value:         Zero Flag
;
; Local Variables:      None
; Shared Variables:     head, tail, length
; Global Variables:     None
;
; Input:                Queue in Memory
; Output:               None
;
; Error Handling:       None
;
; Algorithms:           Queue Wrapping: tail + 1 AND length - 1 = head
; Data Structures:      Queue
;
; Limitation:           Assumes pointers passed are valid
; Known Bugs:           None 
;
; Registers Changed:    None
; Stack Depth:          None
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/01/13
;
; Pseudo Code
; QueueFull(a)
;
; passed_address;                         //Passed by SI
;
; //Checking for cylcing
; 
; if ((queue.tail + 1 AND queue.length - 1) == queue.head) {   
;         return ZeroFlag = 1;            
; }
; else {
;        return ZeroFlag = 0;
; }


QueueFull       PROC        NEAR
                PUBLIC      QueueFull

CheckFull:                              ; Queue Wrap: tail+1 AND length-1 = head
        PUSH    BX                      ; Prevent registers trashing
        PUSH    CX
        MOV     BX, [SI].tail           ; Get Tail pointer 
        ADD     BX, 1                   ;
        MOV     CX, [SI].len            ; Get Length of queue
        SUB     CX, 1
        AND     BX, CX                  ; 
        CMP     BX, [SI].head           ; See if position is same as head or not
        POP     CX                      ; Get back old register values
        POP     BX    
        RET                             ; If yes, zero flag is set and return  

QueueFull	ENDP




; Dequeue
;
; Description:          This function removes either an 8 bit or a 16 bit value 
;                       from the head of the queue at the passed address (a) and
;                       returns it in either AL (8 bits) or AX (16 bits).  If 
;                       the queue is empty, it waits until the queue has a value
;                       to be removed and returned.  This function does not 
;                       return until then.  The address of the queue is passed 
;                       in by SI.
;
; Operation:            This function removes and returns an 8 bit or a 16 bit
;                       value of the head of the queue.  However, if the queue 
;                       is empty it waits until there is a value to be removed 
;                       and return.  Otherwise, it doesn’t return.  The address 
;                       of the queue is passed in by SI.  Once the value is 
;                       returned, the position of the head pointer increments 
;                       since we have a different first element.
;
; Arguments:            Address (SI)
; Return Value:         Value at head.  AX (Word) or AL (Byte)
;
; Local Variables:      None
; Shared Variables:     head, array, length, size
; Global Variables:     None
;
; Input:                Queue in Memory
; Output:               None
;
; Error Handling:       None
;
; Algorithms:           Queue Wrapping: (head + 1) mod length
; Data Structures:      Queue
;
; Limitation:           Assumes pointers passed are valid
; Known Bugs:           None 
;
; Registers Changed:    None
; Stack Depth:          None
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/01/31
;
; Dequeue(a)
;
; address;                                //Passed by SI
;
; while (QueueEmpty == true) {            //If queue is empty, do nothing
; }                                       //Other wise continue
;
; if (queue.size == 1) {                  //If byte        
;        return queue.array[queue.head]   //Return by AL
; }
; if (queue.size == 2) {                  //If word
;        return queue.array[queue.head]   //Return by Ax
; }
;
; queue.head = queue.head + 1 AND queue.length - 1;                                         

Dequeue         PROC        NEAR
                PUBLIC      Dequeue

CheckQueueEmpty:
        CALL    QueueEmpty              ; Call QueueEmpty to check
        JZ      CheckQueueEmpty         ; If queue is empty, do nothing.
        PUSH    BX                      ; Prevent Registers trashing
        PUSH    CX
        CMP     [SI].siz, BYTE_SIZE     ; Check if queue is for Bytes or Words        
        JZ      ReturnByte              ; If size is 1, queue is for byte
        JNZ     ReturnWord              ; otherwise, queue is word

ReturnByte:                             ; Return AL for Bytes
        MOV     BX, WORD PTR [SI].head             
        MOV     AL, [SI].array[BX]      ; Get value of where head pointer is  
        JMP     EndDequeue

ReturnWord:                             ; Return AX for Words 
        MOV     BX, WORD PTR [SI].head  ; Get Head pointer
        SHL     BX, 1                   ;  Multiply by 2 because of WORD QUEUE 
        MOV     AX, WORD PTR [SI].array[BX] ; Get value of where head pointer is
        ;JMP     EndDequeue

EndDequeue:                             ; Increment head pointer by 1
        MOV     BX, [SI].head           ; using queue wrap algorithm
        ADD     BX, 1
        MOV     CX, [SI].len
        SUB     CX, 1
        AND     BX, CX                  
        MOV     [SI].head, BX           ; New Head Pointer
        POP     CX                      ; Get old register values
        POP     BX
        RET

Dequeue 	ENDP




; Enqueue
;
; Description:          This function adds the passed 8 bit or 16 bit value (b) 
;                       to the tail of the queue at the passed address (a).  If 
;                       the queue is full, it waits until the queue has a space 
;                       before adding the value to the queue.  This function 
;                       does not return until then.  The address of the queue is
;                       passed in by SI while the value to put into queue is 
;                       passed in by AL (8 bits) or AX (16 bits).
;
; Operation:            This function adds the passed in 8 bit of 16 bit value 
;                       to the end of the queue.  If the queue is full it waits 
;                       until there is space to put the value in.  Otherwise, it
;                       doesn’t return. Once the value is returned, the position
;                       of the tail pointer increments since the next free space
;                       in the queue moved.  The address of the queue is passed
;                       in by SI while the value to put in queue is passed in by
;                       AL (8 bits) or AX (16 bits).
;
; Arguments:            Address (SI), Value (AX or AL)      
; Return Value:         None
;
; Local Variables:      None
; Shared Variables:     tail, length, array
; Global Variables:     None
;
; Input:                Queue in Memory
; Output:               None
;
; Error Handling:       None
;
; Algorithms:           Queue Wrapping: tail + 1 AND length - 1 
; Data Structures:      Queue
;
; Limitation:           Assumes pointers passed are valid
; Known Bugs:           None 
;
; Registers Changed:    None
; Stack Depth:          None
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/01/31
;
; Pseudo Code
; Enqueue (a, b)
;
; address;                                //Passed by SI
; value;                                  //Passed by AX (word) or AL (byte)
;
; while (QueueFull == true) {             //Do nothing if queue is full
; }
; 
; queue.array[queue.tail] = value;
; 
; queue.tail = queue.tail + 1 AND queue.length - 1; 
;
; return;


Enqueue         PROC        NEAR
                PUBLIC      Enqueue

CheckQueueFull:
        CALL    QueueFull               ; Call QueueFull to check
        JZ      CheckQueueFull          ; If queue is full, keep waiting.
        PUSH    BX                      ; Prevent register trashing
        PUSH    CX
        CMP     [SI].siz, BYTE_SIZE     ; Check if queue is for Bytes or Words        
        JZ      EnqueueByte             ; If size is 1, queue is for byte
        JNZ     EnqueueWord             ; otherwise, queue is word

EnqueueByte:                            ; If queue is for Bytes
        MOV     BX, WORD PTR [SI].tail
        MOV     [SI].array[BX], AL      ; Put AL in queue at tail pointer 
        JMP     EndEnqueue              

EnqueueWord:                            ; If queue is for Words
        MOV     BX, WORD PTR [SI].tail  ; Get Tail Pointer
        SHL     BX, 1                   ; Multiply by 2 because of WORD QUEUE
        MOV     WORD PTR [SI].array[BX], AX    ; Put AX in queue at tail pointer
        ;JMP     EndEnqueue

EndEnqueue:                             ; Increment tail pointer by 1
        MOV     BX, [SI].tail           ; using queue wrap algorithm
        ADD     BX, 1
        MOV     CX, [SI].len
        SUB     CX, 1
        AND     BX, CX                  
        MOV     [SI].tail, BX           ; New Tail Pointer
        POP     CX                      ; Get back old register values
        POP     BX
        RET

Enqueue 	ENDP


CODE    ENDS

END