        NAME  SERIAL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   SerialTable                              ;
;                             Tables of Serial values                        ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ParityTable:          Values for parity
;
; Revision History:
;       11/29/13   Sith Domrongkitchaiporn   Initial Version
;

CGROUP  GROUP   CODE

CODE    SEGMENT PUBLIC 'CODE'

; ParityTable
;
; Description:      This table contains the values to OR with LCR for each 
;                   parity.
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/21/13
;

ParityTable     LABEL   BYTE
                PUBLIC  ParityTable

        DB      00000000B       ;No Parity
        DB      00001000B       ;Odd Parity
        DB      00011000B       ;Even Parity
        DB      00111000B       ;Stick Low Parity
        DB      00101000B       ;Stick High Parity


CODE    ENDS

        END
