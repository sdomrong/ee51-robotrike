asm86 main.asm m1 ep db
asm86 serial.asm m1 ep db
asm86 eventh.asm m1 ep db
asm86 queue.asm m1 ep db
asm86 sertab.asm m1 ep db

asm86chk main.asm
asm86chk serial.asm
asm86chk eventh.asm
asm86chk queue.asm
asm86chk sertab.asm


link86 main.obj, serial.obj, eventh.obj, queue.obj, hw7test.obj, sertab.obj
LOC86 main.lnk TO test NOIC AD(SM(CODE(1000H),DATA(400H), STACK(7000H)))

pause