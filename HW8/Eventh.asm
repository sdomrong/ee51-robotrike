NAME  Eventh

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Serial                                  ;
;                              Serial EventHandler                           ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; SerialEvent:          Calls the serial eventhandler
; InitCS:               Initialize the chip select 
; InitInt2:             Initializes the value for INT2.  Serial interrupts uses
;                       INT2.
; InstallHandler:       Install the event handler into the interrupt vector
;                       table                    
; ClrIRQVectors:        Clear any initial handlers in the vector table.
; IllegalEventHandler:  Any strange interrupts will go here.
;
; Revision History:
;       11/30/13   Sith Domrongkitchaiporn   Update comments
;
; local include files
$INCLUDE(SerialH.inc)
$INCLUDE(Int188.inc)

CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP
        
; InitCS
;
; Description:       Initialize the Peripheral Chip Selects on the 80188.
;
; Operation:         Write the initial values to the PACS and MPCS registers.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/09/13

InitCS  PROC    NEAR
        PUBLIC  InitCS
        
        PUSH    AX
        PUSH    DX
        MOV     DX, PACSreg     ;setup to write to PACS register
        MOV     AX, PACSval
        OUT     DX, AX          ;write PACSval to PACS

        MOV     DX, MPCSreg     ;setup to write to MPCS register
        MOV     AX, MPCSval
        OUT     DX, AX          ;write MPCSval to MPCS
        POP     DX
        POP     AX
        RET                     ;done so return


InitCS  ENDP


; InitInt2
;
; Description:       Initialize Int2 for use with Serial.  
;
; Operation:         Initializes INT 2 for serial use.  It is initialize as INT2
;                    having an edge trigger and priority level 1.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       None
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/30/13

InitInt2        PROC    NEAR
                PUBLIC  InitInt2
                
        PUSH    AX
        PUSH    DX
        
        XOR     AX, AX
        
        MOV     AX, INTNCPR1    ;Write appropriate value for INT2
        MOV     DX, INT2CON
        OUT     DX, AX
        
        MOV     DX, EOI         ;send a INT2 EOI
        MOV     AX, INT2EOI
        OUT     DX, AX
        
        POP     DX
        POP     AX
        RET                     ;done so return


InitInt2        ENDP
                
                

; ClrIRQVectors
;
; Description:      This function installs the IllegalEventHandler for all
;                   interrupt vectors in the interrupt vector table.  Note
;                   that all 256 vectors are initialized so the code must be
;                   located above 400H.  The initialization skips  (does not
;                   initialize vectors) from vectors FIRST_RESERVED_VEC to
;                   LAST_RESERVED_VEC.
;
; Arguments:        None.
; Return Value:     None.
;
; Local Variables:  CX    - vector counter.
;                   ES:SI - pointer to vector table.
; Shared Variables: None.
; Global Variables: None.
;
; Input:            None.
; Output:           None.
;
; Error Handling:   None.
;
; Algorithms:       None.
; Data Structures:  None.
;
; Registers Used:   flags
; Stack Depth:      1 word
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/09/13

ClrIRQVectors   PROC    NEAR
                PUBLIC  ClrIRQVectors

InitClrVectorLoop:              ;setup to store the same handler 256 times
        PUSH    AX
        PUSH    CX
        PUSH    SI
        PUSH    ES
        XOR     AX, AX          ;clear ES (interrupt vectors are in segment 0)
        MOV     ES, AX
        MOV     SI, 0           ;initialize SI to skip Reserved interrupts

        MOV     CX, 256         ;up to 256 vectors to initialize


ClrVectorLoop:                  ;loop clearing each vector
                                ;check if should store the vector
        CMP     SI, 4 * FIRST_RESERVED_VEC
        JB      DoStore	        ;if before start of reserved field - store it
        CMP     SI, 4 * LAST_RESERVED_VEC
        JBE     DoneStore       ;if in the reserved vectors - don't store it
        ;JA     DoStore		;otherwise past them - so do the store

DoStore:                            ;store the vector
        MOV     ES: WORD PTR [SI], OFFSET(IllegalEventHandler)
        MOV     ES: WORD PTR [SI + 2], SEG(IllegalEventHandler)

DoneStore:                          ;done storing the vector
        ADD     SI, 4               ;update pointer to next vector

        LOOP    ClrVectorLoop       ;loop until have cleared all vectors
        ;JMP    EndClrIRQVectors    ;and all done


EndClrIRQVectors:                   ;all done, return
        POP     ES
        POP     SI
        POP     CX
        POP     AX
        RET


ClrIRQVectors   ENDP




; IllegalEventHandler
;
; Description:       This procedure is the event handler for illegal
;                    (uninitialized) interrupts.  It does nothing - it just
;                    returns after sending a non-specific EOI.
;
; Operation:         Send a non-specific EOI and return.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       2 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/13/13

IllegalEventHandler     PROC    NEAR
                        PUBLIC  IllegalEventHandler

        NOP                             ;do nothing (can set breakpoint here)

        PUSH    AX                      ;save the registers
        PUSH    DX

        MOV     DX, EOI                 ;send a non-specific EOI to the
        MOV     AX, EOINSPEC            ;interrupt controller to clear out
        OUT     DX, AX                  ;the interrupt that got us here

        POP     DX                      ;restore the registers
        POP     AX

        IRET                            ;and return


IllegalEventHandler     ENDP

CODE ENDS

END
