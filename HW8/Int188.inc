;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Int188                                  ;
;                         80188 Interrupt Definitions                        ;
;                                 Include File                               ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This file contains the definitions for the 80188 Interrupt Controller
;
;
; Revision History
;       11/05/13   Sith Domrongkitchaiporn  Initial Version
;      

; Address
PeriphBase      EQU     0FF00H               ;Base address of all peripherals

; OFFSET
; Interrupt Control Register 
INT3CON         EQU     PeriphBase + 3EH     ; I3CON Register (non cascade)
INT2CON         EQU     PeriphBase + 3CH     ; I2CON Register (non cascade)
INT1CON         EQU     PeriphBase + 3AH     ; I1CON Register (cascadable)
INT0CON         EQU     PeriphBase + 38H     ; I0CON Register (cascadable)

; Direct Memory Address
DMA0CON         EQU     PeriphBase + 34H     ; DMA0CON Register
DMA1CON         EQU     PeriphBase + 36H     ; DMA1CON Register

; Current Interrupt Register
INTSTAT         EQU     PeriphBase + 30H     ; INTSTS Register
INTREQ          EQU     PeriphBase + 2EH     ; REQST Register
INSERV          EQU     PeriphBase + 2CH     ; INSERV Register

; Interrupt Masking
PRIORMASK       EQU     PeriphBase + 2AH     ; PRIMSK Register
INTMASK         EQU     PeriphBase + 28H     ; IMASK Register

; Poll Interrupt
POLLSTAT        EQU     PeriphBase + 26H     ; POLSTS Register
POLL            EQU     PeriphBase + 24H     ; POLL Register

; End of Interrupt
EOI             EQU     PeriphBase + 22H     ; EOI Register


; Register Definition

; Direct Memory Address
DMAMSKON        EQU     0000000000001000B    ; Mask (Disable) Interrupt 
DMAPRI          EQU     0000000000000001B    ; Priority Lv1
DMAPRII         EQU     0000000000000010B    ; Priority Lv2
DMAPRIV         EQU     0000000000000101B    ; Priority LV4

; Interrupt Control Register (NC)
INTNCLVTRIG     EQU     0000000000010000B    ; Level Triggering
INTNCMSK        EQU     0000000000001000B    ; Mask (Disable) Interrupt 
INTNCPR1        EQU     0000000000000001B    ; Priority Lv1
INTNCPR2        EQU     0000000000000010B    ; Priority Lv2
INTNCPR4        EQU     0000000000000100B    ; Priority Lv4

; Interrupt Control Register (C)
INTCNEST        EQU     0000000001000000B    ; Enable special full nested mode
INTCC           EQU     0000000000100000B    ; Enable Casecode mode
INTCLVTRIG      EQU     0000000000010000B    ; Level Triggering
INTCMSK         EQU     0000000000001000B    ; Mask (Disable) Interrupt
INTCPR1         EQU     0000000000000001B    ; Priority Lv1
INTCPR2         EQU     0000000000000010B    ; Priority Lv2
INTCPR4         EQU     0000000000000100B    ; Priority Lv4

; Interrupt Request Register
INTREQINT3      EQU     0000000010000000B    ; Pending request INT3
INTREQINT2      EQU     0000000001000000B    ; Pending request INT2
INTREQINT1      EQU     0000000000100000B    ; Pending request INT1
INTREQINT0      EQU     0000000000010000B    ; Pending request INT0
INTREGDMA1      EQU     0000000000001000B    ; Pending request DMA1
INTREGDMA0      EQU     0000000000000100B    ; Pending request DMA0
INTREQTMR       EQU     0000000000000001B    ; Pending request timer

; Interrupt Mask Register                    ; Mask Individual source
INTMASKINT3     EQU     0000000010000000B    ; Mask INT3 request
INTMASKINT2     EQU     0000000001000000B    ; Mask INT2 request
INTMASKINT1     EQU     0000000000100000B    ; Mask INT1 request
INTMASKINT0     EQU     0000000000010000B    ; Mask INT0 request
INTMASKDMA1     EQU     0000000000001000B    ; Mask DMA1 request
INTMASKDMA0     EQU     0000000000000100B    ; Mask DMA0 request
INTMASKTMR      EQU     0000000000000001B    ; Mask Interrupt from timers

; Priority Mask Register
PRIORMASKPRI    EQU     0000000000000111B    ; Priority mask (used with AND)

; Inservice Register
INSERVINT3      EQU     0000000010000000B    ; Servicing INT3 request
INSERVINT2      EQU     0000000001000000B    ; Servicing INT2 request
INSERVINT1      EQU     0000000000100000B    ; Servicing INT1 request
INSERVINT0      EQU     0000000000010000B    ; Servicing INT0 request
INSERVDMA1      EQU     0000000000001000B    ; Servicing DMA1 request
INSERVDMA0      EQU     0000000000000100B    ; Servicing DMA0 request
INSERVTMR       EQU     0000000000000001B    ; Servicing Interrupt from timers

; POLL REGISTER                              ; Check and acknowledge
POLLINTREQ      EQU     1000000000000000B    ; Indicate pending interrupt
POLLVECT        EQU     0000000000011111B    ; BIT mask for vector table
                                             ; (AND with highest priority)

; Poll Status Register                       ; Check only
POLLSTATINTREQ  EQU     1000000000000000B    ; Indicate pending interrupt
POLLSTATVECT    EQU     0000000000011111B    ; BIT mask for vector table
                                             ; (AND with highest priority)
; End of Interrupt Register
EOINSPEC        EQU     1000000000000000B    ; Issue non specific EOI
EOIVECT         EQU     0000000000011111B    ; Bit mask to send EOI to
                                             ; (AND with highest priority)  
; Interrupt Status Register
INTSTATHALT     EQU     1000000000000000B    ; Suspend DMA activity
INTSTATTMR2     EQU     1000000000000100B    ; Pending interrupt from Timer2   
INTSTATTMR1     EQU     1000000000000010B    ; Pending interrupt from Timer1 
INTSTATTMR0     EQU     1000000000000001B    ; Pending interrupt from Timer0 

 
