NAME    ParseF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Parser                                  ;
;                               Parser Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; The parser function uses state machines as a way of knowing what to do with
; each next input.  The main function here is ParseSerialChar.  The rest are 
; helper functions for dealing with either state machines or inputs.
;
; CommandTable:         Offsets for executing commands
; ParseSerialChar:      Takes an input and see what to do.  It also updates the
;                       state machine
; StateTable:           Consists of the next state to go to and the action to do
; GetMPToken:           Convert input into a Token.
; TokenTable:           Matches each input to a Token
; ParseError:           Returns AX = 1 if there's an incorrect input into state
;                       machine
; ParseStoreDigit:      Stores the input digits in a buffer
; ParseStoreCommand:    Store the executing command in a shared variable
; ParseNOP:             NOP
; ParseStoreSign:       Stores the sign of the input in a shared variable
; ParseInit:            Initializes the buffer and the shared variables.  Also 
;                       resets the state machine to initial state
; ParseExecute:         Converts the input digits into a value with sign.  Calls
;                       the executing command based on the Command input
; SetAbsoluteSpeed:     Sets the absolute speed
; SetRelativeSpeed:     Gets current speed add input to make new speed
; SetDirection:         Get current direction and adds input for new angle. 
; RotateTurretAngle:    Rotates the turret angle.  If there is sign, the angle
;                       is relative.
; SetTurretElev:        Set the absolute angle of elevation of turret
; FireLaser:            Turns laser on
; LaserOff:             Turns the laser off       
;
; Revision History:
;       12/06/13   Sith Domrongkitchaiporn   Initial Revision
;       12/07/13   Sith Domrongkitchaiporn   Finished coding
;       12/08/13   Sith Domrongkitchaiporn   9 tests left
;       12/09/13   Sith Domrongkitchaiporn   3 tests left
;       12/10/13   Sith Domrongkitchaiporn   All Working        
;       12/11/13   Sith Domrongkitchaiporn   Update Comments

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP

$INCLUDE (parser.inc)
$INCLUDE (GenMac.inc)        

EXTRN   SetMotorSpeed:NEAR
EXTRN   GetMotorSpeed:NEAR
EXTRN   SetLaser:NEAR
EXTRN   SetRelTurretAngle:NEAR
EXTRN   SetTurretElevation:NEAR
EXTRN   SetTurretAngle:NEAR
EXTRN   GetMotorDirection:NEAR

; CommandTable 
;
; Description:          This is a table of offsets that could be called while 
;                       executing parser.
;
; Notes:                Function offsets are words.
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13
        
CommandTable    Label   WORD

        DW      OFFSET(SetAbsoluteSpeed)
        DW      OFFSET(SetRelativeSpeed)
        DW      OFFSET(SetDirection)
        DW      OFFSET(RotateTurretAngle)
        DW      OFFSET(SetTurretElev)
        DW      OFFSET(FireLaser)
        DW      OFFSET(LaserOff)


; ParseSerialChar(c)
;
; Description:          The function is passed in a character c.  The character
;                       is passed in by AL.  The function returns the status of 
;                       the parsing operations in AX.  0 is returned if there
;                       are no parsing errors due to passed in character.  1 is
;                       returned if there is a parsing error due to the passed 
;                       in character.
;
; Operation:            The function acts like a state machine.  It will call
;                       different functions depending on the next input.  The 
;                       functions that it may call next are Error, StoreDigit,
;                       StoreCommand, NOP, StoreSign, Ignore, and Execute.
;
; Arguments:            char - AL
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           State Machine
;
; Data Structures:      None
;
; Limitations:          Assumes correct input from serial
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/07/13

ParseSerialChar  PROC    NEAR
                 PUBLIC  ParseSerialChar

InitParsing:                    
        PUSH    BX                      ;Save register values 
        PUSH    CX
        PUSH    DX

        MOV     CL, state               ;Restore last state

DoNextToken:                    
        CALL    GetMPToken              ;Get the token type and value
        MOV     DH, AH                  ;and save them in DH and CH
        MOV     CH, AL

ComputeTransition:           
        MOV     AL, NUM_TOKEN_TYPES     
        MUL     CL                      ;AX is start of row for current state
        ADD     AL, DH                  ;get the actual transition
        ADC     AH, 0                   ;propagate low byte carry into high byte

        IMUL    BX, AX, SIZE TRANSITION_ENTRY   ;Convert to table offset

DoActions:                
        MOV     AL, CH                  ;get token value back for actions
        
        CALL    CS:StateTable[BX].ACTION1       ;do the actions

DoTransition:                           ;Get Next state
        MOV     CL, CS:StateTable[BX].NEXTSTATE
        MOV     state, CL               ;Store state in memory

EndParseFP:                             ;Done Parsing
        POP     DX                      ;Restore old register values
        POP     CX
        POP     BX
        RET


ParseSerialChar ENDP




; StateTable
;
; Description:      This is the state transition table for the state machine.
;                   Each entry consists of the next state and actions for that
;                   transition.  The rows are associated with the current
;                   state and the columns with the input type.
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    12/06/13


TRANSITION_ENTRY        STRUC           ;structure used to define table
    NEXTSTATE   DB      ?               ;the next state for the transition
    ACTION1     DW      ?               ;Action for the transition
TRANSITION_ENTRY      ENDS


;define a macro to make table a little more readable
;macro just does an offset of the action routine entries to build the STRUC
%*DEFINE(TRANSITION(nxtst, act1))  (
    TRANSITION_ENTRY< %nxtst, OFFSET(%act1) >
)


StateTable    LABEL    TRANSITION_ENTRY

    ;Current State = ST_INITIAL                 Input Token Type
    %TRANSITION(ST_COMA, ParseStoreCommand)     ;TOKEN_COMA
    %TRANSITION(ST_COMN, ParseStoreCommand)     ;TOKEN_COMN
    %TRANSITION(ST_COMS, ParseStoreCommand)     ;TOKEN_COMS
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_POSSIGN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_NEGSIGN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseNOP)           ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_OTHER
    %TRANSITION(ST_INITIAL, ParseNOP)           ;TOKEN_IGNORE
    
    ;Current State = ST_COMA                    Input Token Type
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMA
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMS
    %TRANSITION(ST_SIGN, ParseStoreSign)        ;TOKEN_POSSIGN
    %TRANSITION(ST_SIGN, ParseStoreSign)        ;TOKEN_NEGSIGN
    %TRANSITION(ST_DIGIT, ParseStoreDigit)      ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_OTHER
    %TRANSITION(ST_COMA, ParseNOP)              ;TOKEN_IGNORE

    ;Current State = ST_SIGN                    Input Token Type
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMA
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMS
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_POSSIGN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_NEGSIGN
    %TRANSITION(ST_DIGIT, ParseStoreDigit)      ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_OTHER
    %TRANSITION(ST_SIGN, ParseNOP)              ;TOKEN_IGNORE

    ;Current State = ST_DIGIT                   Input Token Type
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMA
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMS
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_POSSIGN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_NEGSIGN
    %TRANSITION(ST_DIGIT, ParseStoreDigit)      ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseExecute)       ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_OTHER
    %TRANSITION(ST_DIGIT, ParseNOP)             ;TOKEN_IGNORE
    
    ;Current State = ST_COMS                    Input Token Type
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMA
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_COMS
    %TRANSITION(ST_SIGN, ParseStoreSign)        ;TOKEN_POSSIGN
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_NEGSIGN
    %TRANSITION(ST_DIGIT, ParseStoreDigit)      ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)         ;TOKEN_OTHER
    %TRANSITION(ST_COMS, ParseNOP)              ;TOKEN_IGNORE
    
    ;Current State = ST_COMN                    Input Token Type
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_COMA
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_COMN
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_COMS
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_POSSIGN
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_NEGSIGN
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_DIGIT
    %TRANSITION(ST_INITIAL, ParseExecute)      ;TOKEN_RET
    %TRANSITION(ST_INITIAL, ParseError)        ;TOKEN_OTHER
    %TRANSITION(ST_COMN, ParseNOP)             ;TOKEN_IGNORE


; GetMPToken
;
; Description:      This procedure returns the token class and token value for
;                   the passed character.  The character is truncated to
;                   7-bits.
;
; Operation:        Looks up the passed character in two tables, one for token
;                   types or classes, the other for token values.
;
; Arguments:        AL - character to look up.
; Return Value:     AL - token value for the character.
;                   AH - token type or class for the character.
;
; Local Variables:  BX - table pointer, points at lookup tables.
; Shared Variables: None.
; Global Variables: None.
;
; Input:            None.
; Output:           None.
;
; Error Handling:   None.
;
; Algorithms:       Table lookup.
; Data Structures:  Two tables, one containing token values and the other
;                   containing token types.
;
; Registers Used:   AX
; Stack Depth:      0 words.
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    12/02/13

GetMPToken    PROC    NEAR

        PUSH    BX                      ;Save registers

InitGetMPToken:                         ;setup for lookups
        AND    AL, TOKEN_MASK           ;strip unused bits (high bit)
        MOV    AH, AL                   ;and preserve value in AH


TokenTypeLookup:                        ;get the token type
        MOV     BX, OFFSET(TokenTypeTable)   ;BX points at table
        XLAT    CS:TokenTypeTable       ;have token type in AL
        XCHG    AH, AL                  ;token type in AH, character in AL

TokenValueLookup:                       ;get the token value
        MOV     BX, OFFSET(TokenValueTable)  ;BX points at table
        XLAT    CS:TokenValueTable      ;have token value in AL


EndGetMPToken:                          ;done looking up type and value
        POP     BX                      ;Restore register
        RET


GetMPToken    ENDP




; Token Tables
;
; Description:      This creates the tables of token types and token values.
;                   Each entry corresponds to the token type and the token
;                   value for a character.  Macros are used to actually build
;                   two separate tables - TokenTypeTable for token types and
;                   TokenValueTable for token values.
;                   Tokens: COMA, COMN, COMS, POSSIGN, NEGSIGN, DIGIT, RET and
;                           OTHER
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    12/07/13

%*DEFINE(TABLE)  (
        %TABENT(TOKEN_OTHER, 0)         ;<null>  (end of string)
        %TABENT(TOKEN_OTHER, 1)         ;SOH
        %TABENT(TOKEN_OTHER, 2)         ;STX
        %TABENT(TOKEN_OTHER, 3)         ;ETX
        %TABENT(TOKEN_OTHER, 4)         ;EOT
        %TABENT(TOKEN_OTHER, 5)         ;ENQ
        %TABENT(TOKEN_OTHER, 6)         ;ACK
        %TABENT(TOKEN_OTHER, 7)         ;BEL
        %TABENT(TOKEN_OTHER, 8)         ;backspace
        %TABENT(TOKEN_IGNORE, 9)        ;TAB
        %TABENT(TOKEN_IGNORE, 10)       ;new line
        %TABENT(TOKEN_OTHER, 11)        ;vertical tab
        %TABENT(TOKEN_OTHER, 12)        ;form feed
        %TABENT(TOKEN_RET, 0)           ;Return (carriage return)
        %TABENT(TOKEN_OTHER, 14)        ;SO
        %TABENT(TOKEN_OTHER, 15)        ;SI
        %TABENT(TOKEN_OTHER, 16)        ;DLE
        %TABENT(TOKEN_OTHER, 17)        ;DC1
        %TABENT(TOKEN_OTHER, 18)        ;DC2
        %TABENT(TOKEN_OTHER, 19)        ;DC3
        %TABENT(TOKEN_OTHER, 20)        ;DC4
        %TABENT(TOKEN_OTHER, 21)        ;NAK
        %TABENT(TOKEN_OTHER, 22)        ;SYN
        %TABENT(TOKEN_OTHER, 23)        ;ETB
        %TABENT(TOKEN_OTHER, 24)        ;CAN
        %TABENT(TOKEN_OTHER, 25)        ;EM
        %TABENT(TOKEN_OTHER, 26)        ;SUB
        %TABENT(TOKEN_OTHER, 27)        ;escape
        %TABENT(TOKEN_OTHER, 28)        ;FS
        %TABENT(TOKEN_OTHER, 29)        ;GS
        %TABENT(TOKEN_OTHER, 30)        ;AS
        %TABENT(TOKEN_OTHER, 31)        ;US
        %TABENT(TOKEN_IGNORE, ' ')      ;space
        %TABENT(TOKEN_OTHER, '!')       ;!
        %TABENT(TOKEN_OTHER, '"')       ;"
        %TABENT(TOKEN_OTHER, '#')       ;#
        %TABENT(TOKEN_OTHER, '$')       ;$
        %TABENT(TOKEN_OTHER, 37)        ;percent
        %TABENT(TOKEN_OTHER, '&')       ;&
        %TABENT(TOKEN_OTHER, 39)        ;'
        %TABENT(TOKEN_OTHER, 40)        ;open paren
        %TABENT(TOKEN_OTHER, 41)        ;close paren
        %TABENT(TOKEN_OTHER, '*')       ;*
        %TABENT(TOKEN_POSSIGN, POSITIVE) ;+  (POSSIGN)
        %TABENT(TOKEN_OTHER, 44)        ;,
        %TABENT(TOKEN_NEGSIGN, NEGATIVE) ;-  (NEGSIGN)
        %TABENT(TOKEN_OTHER, 0)         ;.  
        %TABENT(TOKEN_OTHER, '/')       ;/
        %TABENT(TOKEN_DIGIT, 0)         ;0  (digit)
        %TABENT(TOKEN_DIGIT, 1)         ;1  (digit)
        %TABENT(TOKEN_DIGIT, 2)         ;2  (digit)
        %TABENT(TOKEN_DIGIT, 3)         ;3  (digit)
        %TABENT(TOKEN_DIGIT, 4)         ;4  (digit)
        %TABENT(TOKEN_DIGIT, 5)         ;5  (digit)
        %TABENT(TOKEN_DIGIT, 6)         ;6  (digit)
        %TABENT(TOKEN_DIGIT, 7)         ;7  (digit)
        %TABENT(TOKEN_DIGIT, 8)         ;8  (digit)
        %TABENT(TOKEN_DIGIT, 9)         ;9  (digit)
        %TABENT(TOKEN_OTHER, ':')       ;:
        %TABENT(TOKEN_OTHER, ';')       ;;
        %TABENT(TOKEN_OTHER, '<')       ;<
        %TABENT(TOKEN_OTHER, '=')       ;=
        %TABENT(TOKEN_OTHER, '>')       ;>
        %TABENT(TOKEN_OTHER, '?')       ;?
        %TABENT(TOKEN_OTHER, '@')       ;@
        %TABENT(TOKEN_OTHER, 'A')       ;A
        %TABENT(TOKEN_OTHER, 'B')       ;B
        %TABENT(TOKEN_OTHER, 'C')       ;C
        %TABENT(TOKEN_COMA, COMMANDD)   ;D (COMA)
        %TABENT(TOKEN_COMA, COMMANDE)   ;E (COMA)
        %TABENT(TOKEN_COMN, COMMANDF)   ;F (COMN)
        %TABENT(TOKEN_OTHER, 'G')       ;G
        %TABENT(TOKEN_OTHER, 'H')       ;H
        %TABENT(TOKEN_OTHER, 'I')       ;I
        %TABENT(TOKEN_OTHER, 'J')       ;J
        %TABENT(TOKEN_OTHER, 'K')       ;K
        %TABENT(TOKEN_OTHER, 'L')       ;L
        %TABENT(TOKEN_OTHER, 'M')       ;M
        %TABENT(TOKEN_OTHER, 'N')       ;N
        %TABENT(TOKEN_COMN, COMMANDO)   ;O (COMN)
        %TABENT(TOKEN_OTHER, 'P')       ;P
        %TABENT(TOKEN_OTHER, 'Q')       ;Q
        %TABENT(TOKEN_OTHER, 'R')       ;R
        %TABENT(TOKEN_COMS, COMMANDS)   ;S (COMS)
        %TABENT(TOKEN_COMA, COMMANDT)   ;T (COMA)
        %TABENT(TOKEN_OTHER, 'U')       ;U
        %TABENT(TOKEN_COMA, COMMANDV)   ;V (COMA)
        %TABENT(TOKEN_OTHER, 'W')       ;W
        %TABENT(TOKEN_OTHER, 'X')       ;X
        %TABENT(TOKEN_OTHER, 'Y')       ;Y
        %TABENT(TOKEN_OTHER, 'Z')       ;Z
        %TABENT(TOKEN_OTHER, '[')       ;[
        %TABENT(TOKEN_OTHER, '\')       ;\
        %TABENT(TOKEN_OTHER, ']')       ;]
        %TABENT(TOKEN_OTHER, '^')       ;^
        %TABENT(TOKEN_OTHER, '_')       ;_
        %TABENT(TOKEN_OTHER, '`')       ;`
        %TABENT(TOKEN_OTHER, 'a')       ;a
        %TABENT(TOKEN_OTHER, 'b')       ;b
        %TABENT(TOKEN_OTHER, 'c')       ;c
        %TABENT(TOKEN_COMA, COMMANDD)   ;d (COMA)
        %TABENT(TOKEN_COMA, COMMANDE)   ;e (COMA)
        %TABENT(TOKEN_COMN, COMMANDF)   ;f
        %TABENT(TOKEN_OTHER, 'g')       ;g
        %TABENT(TOKEN_OTHER, 'h')       ;h
        %TABENT(TOKEN_OTHER, 'i')       ;i
        %TABENT(TOKEN_OTHER, 'j')       ;j
        %TABENT(TOKEN_OTHER, 'k')       ;k
        %TABENT(TOKEN_OTHER, 'l')       ;l
        %TABENT(TOKEN_OTHER, 'm')       ;m
        %TABENT(TOKEN_OTHER, 'n')       ;n
        %TABENT(TOKEN_COMN, COMMANDO)   ;o
        %TABENT(TOKEN_OTHER, 'p')       ;p
        %TABENT(TOKEN_OTHER, 'q')       ;q
        %TABENT(TOKEN_OTHER, 'r')       ;r
        %TABENT(TOKEN_COMS, COMMANDS)   ;s
        %TABENT(TOKEN_COMA, COMMANDT)   ;t (COMA)
        %TABENT(TOKEN_OTHER, 'u')       ;u
        %TABENT(TOKEN_COMA, COMMANDV)   ;v (COMA)
        %TABENT(TOKEN_OTHER, 'w')       ;w
        %TABENT(TOKEN_OTHER, 'x')       ;x
        %TABENT(TOKEN_OTHER, 'y')       ;y
        %TABENT(TOKEN_OTHER, 'z')       ;z
        %TABENT(TOKEN_OTHER, '{')       ;{
        %TABENT(TOKEN_OTHER, '|')       ;|
        %TABENT(TOKEN_OTHER, '}')       ;}
        %TABENT(TOKEN_OTHER, '~')       ;~
        %TABENT(TOKEN_OTHER, 127)       ;rubout
)

; token type table - uses first byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokentype
)

TokenTypeTable    LABEL   BYTE
        %TABLE


; token value table - uses second byte of macro table entry
%*DEFINE(TABENT(tokentype, tokenvalue))  (
        DB      %tokenvalue
)

TokenValueTable    LABEL       BYTE
        %TABLE


; ParseError
;
; Description:          If there is an error in the input in the state machine,
;                       this function is called.  It sets AX to 1 and returns.
;
; Operation:            Sets AX to 1.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    AX
;
; Stack Depth:          None       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseError      PROC        NEAR
                PUBLIC      ParseError
                
        CALL    ParseInit                       ;Reinitialize Parser                
        MOV     AX, RETURN                      ;Error: AX = 1
        
        RET

ParseError      ENDP

; ParseStoreDigit
;
; Description:          Stores the values that are received in a buffer.  The
;                       values received are from 0 - 9 only.  This function also
;                       keeps track of the length of the number
;
; Operation:            Stores the digit in a buffer and increment length.
;
; Arguments:            Digit - AL
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     len, digitbuf
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes correct input
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseStoreDigit  PROC        NEAR
                 PUBLIC      ParseStoreDigit

        PUSH    BX                              ;Save Register
        
        MOV     BX, 0                           ;Clear BX
        MOV     BL, len                         
        MOV     digitbuf[BX], AL                ;Store value in buffer
        INC     len
        MOV     AX, DONE                        ;Successful in parsing
        
        POP     BX                              ;Restore Register value
        RET

ParseStoreDigit ENDP

; ParseStoreCommand
;
; Description:          If the character passed in is the ASCII for S,V,D,T,E,F,
;                       and O.  This function stores the command in a shared
;                       variable.
;
; Operation:            Stores the character in a shared variable called command
;
; Arguments:            char - AL
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     command
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes correct ASCII input
;
; Known Bugs:           None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/02/13

ParseStoreCommand   PROC        NEAR
                    PUBLIC      ParseStoreCommand

        MOV     command, AL             ;Store command in a 
        MOV     AX, DONE                ;Successful parsing
        RET

ParseStoreCommand       ENDP


; ParseNOP
;
; Description:          This function is for a state which does nothing, but is
;                       not an error.
;
; Operation:            NOP
;
; Arguments:            char - AL
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes correct ASCII input
;
; Known Bugs:           None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseNOP        PROC        NEAR
                PUBLIC      ParseNOP
        
        MOV     AX, DONE                        ;Successful parsing
        RET

ParseNOP        ENDP


; ParseStoreSign
;
; Description:          If the character passed in is the ASCII for -ve.  This
;                       function stores the sign as 1 in a shared variable.  If
;                       the character is +ve in ASCII, the sign is stored as 0.
;
; Operation:            Stores the sign as 1 if -ve or 0 if +ve in a shared 
;                       variable called sign.
;
; Arguments:            sign - AL
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     sign
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes correct ASCII input
;
; Known Bugs:           None
;
; Registers Changed:    AX
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseStoreSign  PROC        NEAR
                PUBLIC      ParseStoreSign

        MOV     sign, AL                        ;Store sign
        MOV     AX, DONE
        RET

ParseStoreSign  ENDP


; ParseInit
;
; Description:          Initializes the shared variables and digitbuf used by
;                       the parser functinos
;
; Operation:            Initializes the shared variables.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     len, sign, digitbuf
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseInit       PROC        NEAR
                PUBLIC      ParseInit
        
        PUSH    BX                              ;Save register value
        MOV     len, 0                          ;Initialize shared variables
        MOV     sign, ABSOLUTE                  ;Numbers are initialized as 
                                                ;absolute values.
        MOV     command, 0
        MOV     state, 0
        MOV     BX, 0                           ;Clear BX for loop ** 

ParseInitClr:
        CMP     BX, BUFFER                      ;Clear all the values in the 
        JA      ParseInitDone                   ;buffer                
        MOV     digitbuf[BX], 0                 
        INC     BX
        JMP     ParseInitClr

ParseInitDone:
        POP     BX                              ;Restore register value
        RET

ParseInit       ENDP

; ParseExecute
;
; Description:          Looks up which function to call and see what arguments 
;                       to pass in.
;
; Operation:            Checks what character is stored in the shared variable
;                       command to see which function to call.  If length is not
;                       0, then we get the number stored in the buffer.  If sign
;                       is 1, we will take the negative of that value.  We then
;                       call the function neccesary from a call table.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     len, sign, digitbuf, val, command
;
; Global Variables:     None
; Input:                Value
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes correct stored values.
;
; Known Bugs:           None
;
; Registers Changed:    AX
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ParseExecute    PROC        NEAR
                PUBLIC      ParseExecute

ParseExecuteInit:
      
        PUSH    BX                              ;Save registes value
        PUSH    CX
        PUSH    DX
        
        MOV     AX, 0                           ;Clear AX and BX for usage
        MOV     BX, 0

ParseConvertLoop:
        CMP     BL, len                         ;Converts the numbers stored in
        JB      ParseExecuteConvert             ;buffers into real values 
        JAE     ParseExecuteSign 

ParseExecuteConvert:
        MOV     CX, 10                          ;CX is our value multiplier
        MOV     DX, 0                           ;Clear DX in case of CX overflow
        MUL     CX                              ;Multiply old value by 10
        
        CMP     DX, 0                           ;If overflow, there's an error
        JNE     ParseExecuteError
        
        MOV     DX, 0                           ;Otherwise, add the number in
        MOV     DL, digitbuf[BX]                ;buffer to our value
        ADD     AX, DX
        
        CMP     sign, NEGATIVE                  ;If the sign is -ve, we need to 
        JE      ParseExecuteNEGOF               ;check for overflow
        JNE     ParseExecutePOSOF
        
ParseExecuteNEGOF:                              ;For -ve cases
        CMP     AX, 32768                       ;If the unsigned value is more 
        JA      ParseExecuteError               ;32768, there's an error        
        JMP     ParseExecuteIncLoop

ParseExecutePOSOF:                              ;For +ve cases,
        CMP     AX, 32767                       ;If the unsigned value is more 
        JA      ParseExecuteError               ;32767, there's an error
 
ParseExecuteIncLoop:
        INC     BX                              ;Increment loop counter
        JMP     ParseConvertLoop

ParseExecuteSign:
        CMP     sign, NEGATIVE                  ;If our value is != -ve,
        JNE     ParseExecuteCall                ;call executing function
        NEG     AX                              ;Otherwise, take the absolute
        JMP     ParseExecuteCall                ;value and then call.

ParseExecuteCall:
        MOV     BX, 0                           
        MOV     BL, command                     ;Get the command
        SHL     BX, 1
        CALL    CS:CommandTable[BX]             
        JMP     ParseExecuteDone
        
ParseExecuteError:                              ;If there's an error
        CALL    ParseError                      ;Call function to deal with
        JMP     ParseExecuteRet                 ;error
        
ParseExecuteDone:        
        CALL    ParseInit                       ;Clear parser for new inputs
        MOV     AX, DONE                       

ParseExecuteRet:        
        POP     DX                              ;Restore old register values
        POP     CX
        POP     BX
       
        RET

ParseExecute    ENDP



; SetAbsoluteSpeed
;
; Description:          Set the motor speed to the passed in value.  Angle is
;                       ignored
;
; Operation:            Call SetMotorSpeed with angle = IGNOREANGLE
;
; Arguments:            absolute speed AX
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None 
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

SetAbsoluteSpeed PROC        NEAR
                 PUBLIC      SetAbsoluteSpeed

        PUSH    BX                              ;Save register Value
        MOV     BX, IGNOREANGLE                 ;There is no change in angle
        CALL    SetMotorSpeed
        POP     BX                              ;Restore register value
        RET
        
SetAbsoluteSpeed ENDP


; SetRelativeSpeed
;
; Description:          Gets the original speed and add it to the new value.  
;                       There are many cases to check for overflow.
;
; Operation:            Get the old speed and add to the relative speed.  This
;                       function then checks for overflow.  There is a MAXSPEED
;                       that the function will take.
;
; Arguments:            Relative Speed AX
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       Value overflowing check.
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

SetRelativeSpeed PROC        NEAR
                 PUBLIC      SetRelativeSpeed
        
        PUSH    BX                              ;Save register values
        MOV     BX, AX                          ;Save it in AX
        CALL    GetMotorSpeed                   ;Get current speed
        
SetRelSpeedSignChk:        
        CMP     sign, NEGATIVE                  ;If passed in value is -ve,
        JE      SetRelSpeedNegative             ;we'll deal with it separtely.
        ADD     AX, BX                          ;Otherwise, add AX and BX
        JC      SetRelMaxSpeed                  ;If the value is exceeded
        CMP     AX, IGNORESPEED                 ;the MAXSPEED, we need to change
        JE      SetRelMaxSpeed                  ;values first
        JMP     SetRelativeSpeedCall            ;Otherwise, we're fine.

SetRelMaxSpeed:                                 ;If new speed > MAXSPEED
        MOV     AX, MAXSPEED                    ;set speed = MAXSPEED
        JMP     SetRelativeSpeedCall
        
SetRelSpeedNegative:                            ;If value is -ve
        NEG     BX                              ;Subtract value        
        SUB     AX, BX                          
        JNC     SetRelativeSpeedCall            ;If new speed is -ve, set new
        ;JC      SetRelMinSpeed                 ;speed to be 0
        
SetRelMinSpeed: 
        MOV     AX, 0        
        
SetRelativeSpeedCall:           
        MOV     BX, IGNOREANGLE                 ;No change in angle
        CALL    SetMotorSpeed
        POP     BX                              ;Restore old register value
        RET
        
SetRelativeSpeed ENDP


; SetDirection
;
; Description:          Sets the new angle by using the passed in value.  The 
;                       angle is relative. 
;
; Operation:            This function gets the current angle and adds in our new
;                       relative angle.
;
; Arguments:            Angle AX
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

SetDirection    PROC        NEAR
                PUBLIC      SetDirection

        PUSH    BX                              ;Save register value
        MOV     BX, AX                          ;Save value in BX
        CALL    GetMotorDirection               ;Get current angle(result in AX)
        ADD     AX, BX                          ;ADD current and new angle
        MOV     BX, MAXANGLE                    
        CMP     sign, NEGATIVE                  ;If our number is -ve
        JE      SetDirectionNeg                 ;we need to use IDIV
        MOV     DX, 0                           ;Clear DX for Division
        DIV     BX                              ;MOD with MAX ANGLE
        JMP     SetDirectionDone

SetDirectionNeg:        
        CWD                                     ;Prepare for IDIV
        IDIV    BX              

SetDirectionDone:
        MOV     BX, DX                          ;Get angle mod 360
        MOV     AX, IGNORESPEED                 ;Speed is ingored
        CALL    SetMotorSpeed
        POP     BX                              ;Restore register
        RET    
        
SetDirection    ENDP


; RotateTurretAngle
;
; Description:          Sets the new angle for the turret by using the passed in
;                       value.  The angle is a relative angle if there is a sign
;
; Operation:            Call SetRelTurretAngle.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

;CALL SetRelTurretAngle(val);

RotateTurretAngle PROC        NEAR
                  PUBLIC      RotateTurretAngle

TurretAngleSignChk:
        CMP     sign, ABSOLUTE                  ;If value is absolute,
        JE      TurretAngleAbsolute             
        JNE     TurretAngleRelative             
        
TurretAngleAbsolute:                            ;If yes
        CALL    SetTurretAngle                 
        JMP     RotateTurretAngleDone
  
        
TurretAngleRelative:                            ;If no
        CALL    SetRelTurretAngle          

RotateTurretAngleDone:                  
        RET

RotateTurretAngle ENDP


; RotateTurretElev
;
; Description:          Sets the new angle for the laser elevation by using the 
;                       passed in value.  The angle is the absolute angle.
;
; Operation:            Call SetTurretElevation.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13


RotateTurretElev  PROC        NEAR
                  PUBLIC      RotateTurretElev

TurretElevChk:
        CMP     AX, MAXELEV                     ;Passed in angle cannot be more       
        JG      TurretElevError                 ;than MAXELEV or lower than 
        CMP     AX, MINELEV                     ;MINELEV
        JL      TurretElevError

SetTurretElev:
        CALL    SetTurretElevation              
        JMP     SetTurretElevDone    
 
TurretElevError:        
        CALL    ParseError

SetTurretElevDone:        
        RET

RotateTurretElev ENDP


; FireLaser
;
; Description:          Turns the laser on.
;
; Operation:            SetLaser to be on.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13


FireLaser       PROC        NEAR
                PUBLIC      FireLaser

        MOV     AX, LASERON
        CALL    SetLaser
        
        RET

FireLaser    ENDP



; LaserOff
;
; Description:          Turns the laser off.
;
; Operation:            SetLaser to LASEROFF.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     val
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

LaserOff        PROC        NEAR
                PUBLIC      LaserOff

        MOV     AX, NOLASER
        CALL    SetLaser

        RET
        
LaserOff        ENDP

CODE    ENDS


; Data

DATA    SEGMENT PUBLIC  'DATA'

state           DB    ?                         ;state in machine
command         DB    ?                         ;command
len             DB    ?                         ;length of digit
sign            DB    ?                         ;Store sign 
digitbuf        DB BUFFER DUP (?)               ;Array used to store the digits


DATA ENDS

END
