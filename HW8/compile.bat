asm86 main.asm m1 ep db
asm86 parsef.asm m1 ep db
asm86 eventh.asm m1 ep db

asm86chk main.asm
asm86chk parsef.asm
asm86chk eventh.asm

link86 main.obj, parsef.obj, eventh.obj, hw8test.obj
LOC86 main.lnk TO test NOIC AD(SM(CODE(1000H),DATA(400H), STACK(7000H)))

pause