        NAME    MAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    MAIN                                    ;
;                            Homework #8 Test Code                           ;
;                                  EE/CS  51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Description:  This program tests the serial function of hw8.  The main
;               loop initializes the parser, event handler and 
;               calls the test code.       
;
; Revision History:
;    12/07/13  Sith Domrongkitchaiporn  initial version
;    12/11/13  Sith Domrongkitchaiporn  update comments   

CGROUP  GROUP   CODE
DGROUP  GROUP   STACK

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP

;external function declarations
        EXTRN   InitINT2:NEAR
        EXTRN   InitCS:NEAR
        EXTRN   ClrIRQVectors:NEAR
        EXTRN   ParseInit:NEAR
        EXTRN   ParseTest:NEAR

START:

MAIN:
        CLI                             ;Clear any initial interrupts
        MOV     AX, STACK               ;initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(TopOfStack)
        
        MOV     AX, DGROUP              ; initialize the data segment
        MOV     DS, AX

        CALL    InitCS                  ;initialize the 80188 chip selects
                                        ;   assumes LCS and UCS already setup

        CALL    ClrIRQVectors           ;clear (initialize) interrupt vector table
        
        CALL    InitInt2
        
        CALL    ParseInit

        ;STI                             ;and finally allow interrupts.
        
        CALL    ParseTest               ;Call test code

Forever:
        JMP     Forever                 ;sit in an infinite loop, nothing to
                                        ;   do in the background routine
        HLT                             ;never executed (hopefully)



CODE    ENDS


; Stack
STACK   SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

END     START   
