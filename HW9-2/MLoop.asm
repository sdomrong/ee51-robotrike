NAME    MotorLoop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                  Motor Loop                                ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; These are functions that the motor side of the mainloop uses.  Mainly, it
; handles getting the events and decide what to do with the events.  There is
; only one types of event that could happen: Serial Event.
;
;       EnqueueEvent:   Enqueues the Keyevent and Keyvalue
;       DequeueEvent:   Dequeues the keyevent and keyvalue and decide what 
;                       action to take.  There are 2 possible events, error or
;                       keypad.
;       MainInit:       Initializes the shared variables, and queues 
;                       necessary for Main Functions.
;
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision
;       12/18/13   Sith Domrongkitchaiporn   Update Comments

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP
      
EXTRN   QueueFull:NEAR
EXTRN   QueueEmpty:NEAR
EXTRN   Dequeue:NEAR
EXTRN   Enqueue:NEAR
EXTRN   QueueInit:NEAR  
EXTRN   ParseSerialChar:NEAR

$INCLUDE (MLoop.inc)        
        

; EnqueueEvent
;
; Description:          This function utilizes ad event queue.  It enqueues the
;                       the key type and key value into a queue.  The key type 
;                       is passed in as AH and the key value is passed as in 
;                       AL.
;
; Operation:            Enqueue the keyevent and keyvalues into an eventqueue.
;
; Arguments:            KeyEvent (AH) and KeyValue (AL)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     full
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/18/13

EnqueueEvent    PROC        NEAR
                PUBLIC      EnqueueEvent
        
        PUSH    SI                              ;Save register
        LEA     SI, event                       ;Get the address of event queue
        CALL    QueueFull                       ;Check to see if queue is full
        JZ      EnqueueEventFull                ;if yes, there's an error
        CALL    Enqueue                         ;Otherwise, enqueue event
        JMP     EnqueueEventDone
        
EnqueueEventFull:
        CALL    MainInit                        ;Reset Hard

EnqueueEventDone:
        POP     SI                              ;Restore register
        RET
        
EnqueueEvent    ENDP


; DequeueEvent
;
; Description:          This function dequeues the eventqueue.  The first value
;                       is the keytype and the second value is the keyvalue.
;
; Operation:            Dequeue the keyevent and keyvalues.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    AX
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/18/13

DequeueEvent    PROC        NEAR
                PUBLIC      DequeueEvent
        
DequeueEventInit:        
        PUSH    AX                              ;Save Registers
        PUSH    BX      
        LEA     SI, event                       ;Get the address of event queue
        CALL    QueueEmpty                      ;Check if queue is empty
        JZ      DequeueEventDone                ;If yes, do nothing
        CALL    Dequeue                         ;otherwise, get the next event
        CMP     AH, CHAREVENT                   ;If it's a character event,
        JE      DequeueChar                     ;figure out the character
        JMP     DequeueEventDone                ;It's an unknown 
        
DequeueChar:
        CALL    ParseSerialChar                 ;Sort out the character
        JMP     DequeueEventDone

DequeueEventDone:
        POP     BX                              ;Restore register value
        POP     AX
        RET
        
DequeueEvent    ENDP


; MainInit
;
; Description:          This function initializes the queues and variables for
;                       the usage of the main loop.  This maybe called if the 
;                       main loop reaches a critical error and requires a reset
;                       hard.
;
; Operation:            None
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

MainInit        PROC        NEAR
                PUBLIC      MainInit
        
        PUSH    SI
        PUSH    BX
        
MainInitInit:
        LEA     SI, event                       ;Get address of queue
        MOV     BX, 1                           ;This is a WORD queue
        CALL    QueueInit
        MOV     SI, 0
        
MainInitDone:        
        POP     BX
        POP     SI
        RET
MainInit        ENDP

CODE    ENDS

; Data

DATA    SEGMENT PUBLIC  'DATA'

event           Q_EVENT <>                       ;Initializing queue 

DATA ENDS

END
