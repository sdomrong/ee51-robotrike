;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Main Loop                                ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; This is an include file for the motor loop
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision

MAX_BYTE_QUEUE  EQU     256                 ; Max size for Byte Queue 
CHAREVENT       EQU     1                   ; Chracter Event Type

Q_EVENT        STRUC                        ; Queue structure
    head        DW      ?                   ; Head position of Queue
    tail        DW      ?                   ; Tail position of the Queue
    len         DW      ?                   ; Length of Queue
    siz         DB      ?                   ; Size of an element in the queue
    array       DB (MAX_BYTE_QUEUE+1) DUP (?) ; Array used to store the queue
Q_EVENT        ENDS
