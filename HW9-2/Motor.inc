;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    MOTORS                                  ;
;                               Motors Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Include file for motor functions
;
; Revision History:
;    11/23/13  Sith Domrongkitchaiporn	initial version
;
; Constants
IGNORESPEED     EQU     65535               ;Ignore speed this is input
IGNOREANGLE     EQU     -32768              ;Ignore angle if this input

ROBOANGLE       EQU     360                 ;Angle robot can go in
NUMMOTOR        EQU     3                   ;No. of motors for RoboTrike
LASEROFF        EQU     0                   
LASERON         EQU     1                   

LASERBIT        EQU     10000000B           ;Turn on bit for laser
PCTRL           EQU     10010000B           ;Write to parallel port
