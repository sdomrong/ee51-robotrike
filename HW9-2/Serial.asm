NAME    Serial

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Serial                                  ;
;                               Serial Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; This file contains functions related to serial.  It contains event handlers 
; for both sending and receiving data.
;
; HandlerTable:         Contains offset for call funcions
; SerialInit:           Initializes Serial
; SerialPutChar:        Puts a character in queue and kickstart if necessary
; SetSerialSize:        Set the wordlength for serial (bit0-1)
; SetSerialStop:        Set stop bit for serial (bit2)
; SetSerialParity:      Set parity bits (bit3-5)
; SetSerialBaud:        Set divisor
; SerialHandler:        Deal with interrupts accordingly by calling functions to
;                       deal with them.
; SerialModem:          Deal with Modem Interrupts
; SerialTHR:            Deal with Transmit holding register empty
; SerialData:           Deal with data interrupts
; SerialError:          Deal with error interrupts
;
; Revision History:
;       11/28/13   Sith Domrongkitchaiporn   Initial Revision
;       11/30/13   Sith Domrongkitchaiporn   Working version   
;       12/01/13   Sith Domrongkitchaiporn   Update Comments
;       12/17/13   Sith Domrongkitchaiporn   Update Descriptions

CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DATA

EXTRN   ParityTable:BYTE
EXTRN   EnqueueEvent:NEAR
EXTRN   QueueFull:NEAR
EXTRN   QueueEmpty:NEAR
EXTRN   Dequeue:NEAR
EXTRN   Enqueue:NEAR
EXTRN   QueueInit:NEAR

$INCLUDE (Serial.inc)
$INCLUDE (Queue.inc)
$INCLUDE (GenMac.inc)

HandlerTable    Label   WORD

        DW      OFFSET(SerialModem)
        DW      OFFSET(SerialTHR)
        DW      OFFSET(SerialData)
        DW      OFFSET(SerialError)


; SerialInit
;
; Description:          This function initializes the queue required for the 
;                       serial.  It initializes the serial by setting the bits.
;                       It also enable interrupts for serial.
;
; Operation:            This function initializes the queue by calling queueinit
;                       Then calls the serial set functions to initialize serial
;                       It also writes to IER to enable interrupts.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               IER
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queue
;
; Limitations:          Assumes that values passed in are valid.  
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SerialInit      PROC        NEAR
                PUBLIC      SerialInit

SerialInitInit:
        PUSH    AX                              ;Save Registers
        PUSH    SI   
        PUSH    BX
        
        LEA     SI, queue                       ;Get address of queue
        MOV     BX, 0                           ;This is a BYTE queue
        CALL    QueueInit

        MOV     BX, INITSIZE                    ;Initialize Size Bits by calling 
        CALL    SetSerialSize                   ;function

        MOV     BX, INITSTOP                    ;Initialize Stop Bit 
        CALL    SetSerialStop

        MOV     AX, INITPARITY                  ;Initialize Parity Bits
        CALL    SetSerialParity
                
        MOV     BX, INITDIV                     ;Initialize Division 
        CALL    SetSerialBaud
 
SerialInitIER:                             
        %READPCB(LCRADR)                        ;Read in LCR
        %CLRBIT(AX, 7)                          ;Set bit 7 for DLAB so LSB and 
                                                ;MSB can be accessed
        %WRITEPCB(LCRADR, AL)                   ;Output LCR

SerialInitIEROut:
        MOV     AL, IERINIT
        %WRITEPCB(IERADR, AL)                   ;Initialize IER for interrupts  
        
        POP     BX
        POP     SI
        POP     AX                              ;Return registers value

        RET

SerialInit      ENDP


; SerialPutChar(c)
;
; Description:          The function outputs the passed character to the serial
;                       channel.  It returns with the carry flag reset if the 
;                       character has been put into the channel's queue.  Other-
;                       wise, return carry flag as set.  The character passed in
;                       by value in AL.  It also kickstart the serial if needed.
;
; Operation:            The function takes the passed in chracter and puts in 
;                       the channel's queue.  If the queue is not full, this
;                       function puts the chracter in the queue and returns with
;                       a reset carry flag.  If the queue is full, it returns 
;                       with a set carry flag.  If the queue was initially empty
;                       this function will kickstart the interrupt by writing to
;                       IER.
;
; Arguments:            character (AL)
;
; Return Value:         Carry Flag
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queue
;
; Limitations:          Assumes that values passed in are valid.  
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/28/13

SerialPutChar   PROC        NEAR
                PUBLIC      SerialPutChar

SerialPutCharInit:
        PUSH    SI                              ;Save Registers
        PUSH    BX

SerialQueueEmptyChk:
        LEA     SI, queue                       ;Get the address of queue
        CALL    QueueEmpty                      ;Check if queue is empty
        JZ      SerialQueueEmpty                
        JNZ     SerialNotEmpty

SerialQueueEmpty:
        MOV     BX, 1                           ;If queue is empty, BX = 1
        JMP     SerialQueueChk

SerialNotEmpty:
        MOV     BX, 0                           ;If queue isn't empty, BX = 0
        ;JMP     SerialQueueChk        

SerialQueueChk:
        CALL    QueueFull                       ;Check if queue is ful
        JZ      SerialPutCharQFull              ;If queue is full, do nothing
        JNZ     SerialEnqueue                   ;if not, we need to enqueue

SerialEnqueue:
        CALL    Enqueue                         ;Enqueue the char in AL
        CMP     BX, 1                           ;If queue was empty before
        JE      SerialKickFlag                  ;We need to kickstart flag
        JNE     SerialPutCharQFree              ;Otherwise, we're done.

SerialKickFlag:
        %READPCB(IERADR)                        ;Read value of LSR
        %SETBIT(AX, 1)                          ;Set bit 5 to generate interrupt
        %WRITEPCB(IERADR, AL)                   ;Write to LSR

SerialPutCharQFree:
        CLC                                     ;Clear carry flag
        JMP     SerialPutCharDone

SerialPutCharQFull:                             ;If cannot enqueue
        STC                                     ;Set the carry flag        

SerialPutCharDone:
        POP     BX                              ;Restore register values
        POP     SI
        RET

SerialPutChar   ENDP


; SetSerialSize(a)
;
; Description:          The function sets the word length of the transmitting or
;                       or receiving signal.  It outputs the bits to LCR.
;
; Operation:            This function looks up what to set for bits 0 and 1 for
;                       LCR by subtracting 5.  This is done because length - 5
;                       corresponds to the values needed to set LCR
;
; Arguments:            Size (BL)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     size
;
; Global Variables:     None
; Input:                None
; Output:               LCR
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Takes arguments of 5-8 bits only
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SetSerialSize   PROC        NEAR
                PUBLIC      SetSerialSize

SetSerialSizeInit:
        PUSH    AX                              ;Save Registers
        PUSH    BX

        %READPCB(LCRADR)                        ;Read in value from LSR
        SUB     BL, 5
        OR      AL, BL                          ;Set bits 0 and 1
        ;SIZE-5 corresponds to the right value to set for size                      
        %WRITEPCB(LCRADR, AL)                   ;Output LSR

        POP     BX                              ;Restore Registers
        POP     AX
        RET

SetSerialSize   ENDP

; SetSerialStop(a)
;
; Description:          The function sets the stop bits needed.  It stores the 
;                       stop bit in a shared variable and output bit2 of LCR. 
;                       The argument passed in is the actual bit needed to be
;                       set.
;
; Operation:            The function stores the size of the stop bits in a 
;                       shared variable.  The stop bits is passed in as either 0
;                       or 1.  0 corresponds to 1 stop bit while 1 could refer
;                       to 1.5 or 2 stop bits.  Then we output bit2 of the LCR 
;                       with the value needed.
;
; Arguments:            Stop (BL)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     stop
;
; Global Variables:     None
; Input:                None
; Output:               LCR
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Takes arguments for as 0 or 1.
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SetSerialStop   PROC        NEAR
                PUBLIC      SetSerialStop

SetSerialStopInit:
        PUSH    AX                              ;Save Register

        SHL     BL, 2                           ;Bit2 is the stop bit
        %READPCB(LCRADR)                        ;Read in LCR value
        OR      AL, BL                          ;Set bit2 
        %WRITEPCB(LCRADR, AL)                   ;Output LCR        

        POP     AX                              ;Return register value        
        RET

SetSerialStop   ENDP


; SetSerialParity(a)
;
; Description:          The function sets the parity bits for the LCR.  This 
;                       function stores the value for odd or even parity or 
;                       stick high/low parity in a shared variable.  It then 
;                       outputs the bits 3-5 to LCR.
;
; Operation:            The function stores which bits to set in a table for
;                       look up. It check which case of parity is passed in and
;                       outputs to bit3-5 of LCR.
;
; Arguments:            Parity (AL)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     parity
;
; Global Variables:     None
; Input:                None
; Output:               LCR
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Only takes in the values corresponding to parity
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SetSerialParity PROC        NEAR
                PUBLIC      SetSerialParity

SetSerialParityInit:
        PUSH    BX                              ;Save register
        
        LEA     BX, ParityTable                 ;Get address of parity table
        XLAT    ParityTable                     ;Get Mask for type of parity
        MOV     BL, AL                          ;Save this mask in BX
        %READPCB(LCRADR)                        ;Read in LCR
        OR      AL, BL                          ;Set bits
        %WRITEPCB(LCRADR, AL)                   ;Output LCR

        POP     BX                              ;Return register value
        RET

SetSerialParity ENDP


; SetSerialBaud
;
; Description:          The function accepts the divisor for the baud rate as
;                       its argument.  It outputs to LSB and MSB.
;
; Operation:            This function takes the divisor# and outputs to LSB and 
;                       MSB.
;
; Arguments:            Divisor (BX)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     baud
;
; Global Variables:     None
; Input:                None
; Output:               LSB and MSB
; 
; Error Handling:       None
;
; Algorithms:           divisor# = frequency / (baud rate*16)
;
; Data Structures:      None
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13


SetSerialBaud   PROC        NEAR
                PUBLIC      SetSerialBaud

SerialBaudInit:
        PUSH    DX                              ;Save register
        PUSH    AX
        PUSHF                                   ;Save flags
        CLI                                     ;Stop interrupts from happening
                                                ;while in this statement        
        %READPCB(LCRADR)                        ;Read in LCR
        %SETBIT(AX, 7)                          ;Set bit 7 for DLAB so LSB and 
                                                ;MSB can be accessed
        %WRITEPCB(LCRADR, AL)                   ;Output LCR

SerialBaudOut:
        MOV     AX, BX
        %WRITEPCB(LSBADR, AL)                   ;Access LSB
        %WRITEPCB(MSBADR, AH)                   ;Access MSB

        %READPCB(LCRADR)                        ;Read LCR
        %CLRBIT(AX, 7)                          ;Clear DLAB bit
        %WRITEPCB(LCRADR, AL)                   ;Output LCR        
    
        POPF                                    ;Restore Flags
        POP     AX                              ;Restore Register values
        POP     DX
        RET

SetSerialBaud   ENDP


; SerialHandler
;
; Description:          The function looks at the IIR to see what kind of
;                       interrupts is pending and deals with it by calling the
;                       functions that deals with that interrupts
;
; Operation:            This function reads pending interrupts until there
;                       are no more and deals with them accordingly by calling
;                       functions.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               Output to serial
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queue
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SerialHandler   PROC        NEAR
                PUBLIC      SerialHandler

SerialHandlerInit:
        PUSH    AX                              ;Save registers
        PUSH    BX       

HandlerIIRRead:
        %READPCB(IIRADR)                        ;Read IIR
        %TESTBIT(AX, 0)                         ;If bit0 = 0
        JZ      SerialDealInt                   ;We need to deal with interrupts
        JNZ     SerialHandlerDone               ;Otherwise, we're done

SerialDealInt:
        MOV     AH, 0
        SHR     AX, 1                           ;Divide by 2 for ease of using
        LEA     BX, HandlerTable                ;Handler table
        %XLATW  ;HandlerTable                    ;Get the address to call
        CALL    AX                              ;Call function
        JMP     HandlerIIRRead                  ;Loop until there are no more
                                                ;interrupts

SerialHandlerDone:                              ;Return saved registers
        POP     BX
        POP     AX
        RET

SerialHandler   ENDP        

; SerialModem
;
; Description:          The function deals with modem interrupts.  Modem 
;                       interrupts are dealth by reading from MSR.       
;
; Operation:            This function deal with modem interrupts by reading the
;                       values from MSR.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SerialModem     PROC        NEAR

SerialModemInit:
        PUSH    AX
        %READPCB(MSRADR)                        ;Clear interrupt by reading
        POP     AX
        RET                                     ;MSR

SerialModem     ENDP

; SerialTHR
;
; Description:          The function deals with transmitting holding register
;                       interrupts.  If there is data in queue, this function
;                       dequeues and outputs that value into the transmit
;                       holding register.     
;
; Operation:            This function deal with transmitting interrupts.  If 
;                       there is data in the queue, this function calls dequeue.
;                       It then outputs to THR.  It also reads from IIR to clear
;                       the interrupts.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SerialTHR       PROC        NEAR

SerialTHRClr:                        
        PUSH    AX                      ;Save Registers value
        PUSH    SI
        %READPCB(LCRADR)                ;Selecting THR by setting DLAB = 0       
        %CLRBIT(AX, 7)
        %WRITEPCB(LCRADR, AL)

SerialTHRQueueChk:
        LEA     SI, queue               ;Get address of queue 
        CALL    QueueEmpty              ;If queue is empty,
        JZ      SerialTHRDone           ;We're done
        ;JNZ     SerialSetTHR            ;Otherwise, we need to dequeue
                                        
SerialSetTHR:            
        CALL    Dequeue                 
        %WRITEPCB(THRADR, AL)           ;Output data to THR

SerialTHRDone:
        %READPCB(IIRADR)                ;read IIR to clear interrupt
        POP     SI                      ;Restore register values
        POP     AX    
        RET

SerialTHR       ENDP


; SerialData
;
; Description:          The function deals with data interrupts.  It reads from
;                       the RBR and calls enqueue event.
;
; Operation:            The function deals with data interrupts.  It reads from
;                       the RBR to clear the interrupt.  It then calls the 
;                       function enqueue event.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13

SerialData      PROC        NEAR

SerialDataClr:                       
        PUSH    AX                      ;Save registers
        
        %READPCB(LCRADR)                ;Selecting RBR by setting DLAB = 0
        %CLRBIT(AX, 7)
        %WRITEPCB(LCRADR, AL)

SerialDataCall:
        %READPCB(RBRADR)                ;Read from RBR to clear interrupt
        MOV     AH, CHAREVENT           ;Need to Enqueue
        CALL    EnqueueEvent            
        
        POP     AX                      ;restore register values
        RET

SerialData  ENDP

; SerialError
;
; Description:          The function deals with error interrupts.  It reads from
;                       the LSR to see what errror it is and enqueueevent.
;
; Operation:            The function deals with error interrupts.  It reads from
;                       the LS to clear the interrupt.  It checks the error
;                       by looping through each error.  It then enqueue event.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None 
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/25/13


SerialError     PROC        NEAR

SerialErrorInit:
        PUSH    CX                      ;Save Registers
        PUSH    BX        
        PUSH    AX
        MOV     CL, 1                   ;Start to check LSR at bit 1

SerialErrorRead:
        %READPCB(LSRADR)                ;Read LSR to clear interrupt

SerialErrorLoop:
        CMP     CL, LSRCHK              ;Check if we have checked every error
        JBE     SerialErrorChk          ;If no, keep checking
        JA      SerialErrorDone         ;Otherwise, we're done.                

SerialErrorChk:
        %TESTBIT(AX, CL)                ;Check if bit is set or not
        JNZ     SerialErrorEnqueue      ;if yes, there's an error
        INC     CL                      ;Otherwise, increment counter and
        JZ      SerialErrorLoop         ;Check other errors
        
SerialErrorEnqueue:
        MOV     BX, AX
        MOV     AH, ERROREVENT          ;If there's an error
        MOV     AL, CL                  ;Record the event
        CALL    EnqueueEvent
        INC     CL                      ;increment counter
        MOV     AX, BX
        JMP     SerialErrorChk          ;check other errors

SerialErrorDone:
        POP     AX                       ;Restore registers
        POP     BX
        POP     CX
        RET

SerialError     ENDP


CODE    ENDS


; Data
DATA    SEGMENT PUBLIC  'DATA'

baudrate        DB    ?                           ;Baud Rate variable
queue           Q_STRUCT <>                       ;Initializing queue 


DATA ENDS

END
