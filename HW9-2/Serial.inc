;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Serial                                  ;
;                               Serial Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Include file for serial functions
;
; Revision History:
;    11/29/13  Sith Domrongkitchaiporn	initial version


;Constants
NOPARITY        EQU     0
ODDPARITY       EQU     1
EVENPARITY      EQU     2
STICKLOW        EQU     3
STICKHIGH       EQU     4

LSRCHK          EQU     4
CHAREVENT       EQU     1
ERROREVENT      EQU     2

;Initialization
INITSIZE        EQU     8
INITSTOP        EQU     1
INITPARITY      EQU     0
INITDIV         EQU     60            ;Desire Baud Rate: 9600                


;Addresses
SERIALADR       EQU     100h

;Offset
IIRADR          EQU     SERIALADR + 2
LCRADR          EQU     SERIALADR + 3
LSRADR          EQU     SERIALADR + 5
MSRADR          EQU     SERIALADR + 6
IERADR          EQU     SERIALADR + 1

MSBADR          EQU     SERIALADR + 1
LSBADR          EQU     SERIALADR       ;LSB is the first register
THRADR          EQU     SERIALADR
RBRADR          EQU     SERIALADR

IERINIT         EQU     00000111B
