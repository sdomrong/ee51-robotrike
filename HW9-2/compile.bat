asm86 Eventh.asm m1 ep db
asm86 MMain.asm m1 ep db
asm86 Motab.asm m1 ep db
asm86 Motor.asm m1 ep db
asm86 MLoop.asm m1 ep db
asm86 ParseF.asm m1 ep db
asm86 Queue.asm m1 ep db
asm86 Serial.asm m1 ep db
asm86 SerTab.asm m1 ep db
asm86 TrigTab.asm m1 ep db



asm86chk Eventh.asm
asm86chk MMain.asm
asm86chk Motab.asm 
asm86chk Motor.asm 
asm86chk MLoop.asm
asm86chk ParseF.asm
asm86chk Queue.asm
asm86chk Serial.asm 
asm86chk SerTab.asm
asm86chk TrigTab.asm

link86 mmain.obj, eventh.obj, Motab.obj, motor.obj, MLoop.obj, ParseF.obj
link86 Queue.obj, serial.obj, SerTab.obj, TrigTab.obj
link86 mmain.lnk, Queue.lnk to test.lnk
LOC86 test.lnk TO test NOIC AD(SM(CODE(1000H),DATA(400H), STACK(7000H)))

pause