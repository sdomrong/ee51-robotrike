        NAME    Dec2String

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   CONVERTS                                 ;
;                             Conversion Functions                           ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; Number Conversions for RoboTrike
; - Dec2String - Converts a signed number into its decimal ASCII representation 
; - Hex2String - Converts an unsigned number to its hexadecimal 
;		 ASCII represetation
;
; Revision History:
;    10/23/13  Sith Domrongkitchaiporn	initial version
;    10/24/13  Sith Domrongkitchaiporn  fix syntax	
;    10/25/13  Sith Domrongkitchaiporn	fix formatting
;    12/14/13  Sith Domrongkitchaiporn  Update Push/Pop for Dec2String   

CGROUP  GROUP   CODE


CODE	SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP

$INCLUDE (Converts.inc)

; Dec2String
;
; Description:		This function converts the number n to a string
;			containing its decimal representation stored at a.  The 
;			output string will have a sign in front.  There will be 
;			zero padding if needed.  The output will have 7 bytes, 
;			all in ASCII.  The digits are stored from bottom to top 
;			of memory.
;
; Operation:	        The function is passed a 16-bit signed value(n) to 
;			convert to decimal (at most 5 digits plus (sign) and 
;	                store as a string.  The sign is stored first while each 
;		        of the digits is determined by taking the quotient of a 
;	   	        division by powers of 10.  This continues by doing the 
;			same thing to the remainder with the next lower power of
;			10.  If quotient is 0, 0 will be stored.  Once all the 
;			digits are stored, <null> terminate will be input into
;			the string.  The resulting string is stored starting at 
;			the memory location indicated by the passed address(a).
;			The number is passed AX by value.  The address(a) is 
;			passed in SI by value and the string is written to 
;			memory starting at DS:SI.  
;
; Arguments:            n (AX) - 16 bit signed value
;			a (SI) - address
;
; Return Value:		None
;
; Local Variables:	Position
;			Power
;			Quotient
; Shared Variables:	None
; Global Variables:	None
;
; Input:		None
; Output:		String in Memory
;
; Error Handling:	None
;
; Algorithms:		If the number has 1 on the top bit, we need to store a 
;			-ve sign in memory and continue to treat the number 
;			as +ve.  If the top bit is zero, we store a +ve sign and
;			nothing else.
;
; Data Structures:	None
;
; Limitations:		Assumes pointer passed is valid.
;
; Known Bugs:		None
;
; Registers Changed:	AX, SI
; Stack Depth:		0
;
; Author:		Sith Domrongkitchaiporn
; Last Modified:	10/23/13

Dec2String      PROC        NEAR
                PUBLIC      Dec2String
    PUSH    BX
    PUSH    CX
    PUSH    DX
    
Dec2StringInit:				; Initializing
	MOV	    BX, 10000		; Start power of 10 or Divisor
	;JMP	Dec2StringChk

Dec2StringChk:
	TEST	AX, 8000H		; Check to see if number is -ve
	;JE	    PositiveNum		; If number is +ve, go to PositiveNum
	JNZ	    NegativeNum		; If -ve, process in NegativeNum

PositiveNum:				; If number is +ve,
    MOV	    byte ptr[SI], '+'	; put '+' sign in memory
    INC 	SI              	; Put pointer at next memory
	JMP	    DecLoopPowerChk

NegativeNum:				; If number is -ve,
	MOV	    byte ptr[SI], '-'	; put '-' sign in memory
    INC 	SI             	 	; Put pointer at next memory
	NEG	    AX			; Make input number a +ve number
	;JMP	DecLoopPowerChk
	
DecLoopPowerChk:				 
	CMP	    BX, 0	        ; While Power > 0
	JLE	    Dec2StringEnd	; If Power < 0, we're done
	;JG	    DigitSelectLoop	; Otherwise, continue to get digits

DigitSelectLoop:
	MOV	    DX, 0		; Clear DX
	DIV	    BX			; Divide n by power for digit
	ADD	    AX, '0'		; '0' is for ASCII	
	MOV	    byte ptr[SI], AL	; Put quotient in memory
	MOV	    CX, DX		; Put remainder in a holder 
	MOV	    AX, BX		; Move power into AX for division
	MOV	    BX, 10		; We need to lessen the power, so
	MOV	    DX, 0		; Clear DX
	DIV	    BX			; Divide power by 10 for next round
	MOV	    BX, AX		; Put next divisor in Power
	MOV 	AX, CX			; Put remainder as new n
	INC	    SI			; Puts pointer at next memory position
	JMP	    DecLoopPowerChk	; Goes to Check if done		

Dec2StringEnd:				; Once Done
	MOV	    byte ptr[SI], ASCII_ZERO; Null Terminate the String
    
    POP     DX
    POP     CX
    POP     BX
	RET
	
Dec2String	ENDP




; Hex2String
;
; Description:		This function converts the number n to a string
;			containing its hexadecimal representation stored at a.
;			There will be zero padding if needed.  The output will 
;			have 7 bytes, all in ASCII.  The digits are stored from 
;			bottom to top of memory.
;
; Operation:		The function is passed a 16-bit unsigned value(n) to 
;			convert to hexadecimal (at most 5 digits plus and 
;			store as a string.  The sign is stored first while each 
;			of the digits is determined by taking the quotient of a 
;			division by powers of 10H.  This continues by doing the 
;			same thing to the remainder with the next lower power of
;			10H.  If quotient is 0, 0 will be stored.  Once all the 
;			digits are stored, <null> terminate will be input into
;			the string.  The resulting string is stored starting at 
;			the memory location indicated by the passed address(a).
;			The number is passed AX by value.  The address(a) is 
;			passed in SI by value and the string is written to 
;			memory starting at DS:SI.
;
; Arguments:		n (AX) - 16 bit unsigned value
;			a (SI) - address
; Return Value:		None
;
; Local Variables:
; Shared Variables:	None
; Global Variables:	None
;
; Input:		None
; Output:		String in Memory
;
; Error Handling:	None
;
; Algorithms:		None		
; Data Structures:	None
;
; Registers Changed: AX, Bx, CX, DX, SI
; Stack Depth:
;
; Author:		Sith Domrongkitchaiporn
; Last Modified:	10/23/13
;
; Pseudo Code
; n, a;					// Arguments
; position = 0;				// Place to put in memory
; power = 1000H;					
;
; while (power > 0) {	           	// Continue loop until we divide every
;	quotient = n/power;		// digit
;	n = n mod power;		// n is now the remainder after division
;					
;	if (quotient > 9) {		// Store A - F instead of 10-15	
;		a[position] = quotient - 10 + 'A';  
;	}
;	else {				// Store input from 0-9
;		a[position] = quotient + '0';	    
;	}
;	power = power/10H;				     
;	position++;
; }
; a[position] = <null>;			// Null terminate after all digits are 
;					// input

Hex2String      PROC        NEAR
                PUBLIC      Hex2String

Hex2StringInit:				; Initializing
	MOV	BX, 1000H		; Start power of 10 or Divisor
	;JMP	HexLoopPowerChk

HexLoopPowerChk:				 
	CMP	BX, 0			; While Power > 0
	JLE	Hex2StringEnd		; If Power < 0, we're done
	;JG	DigitLoop		; Otherwise, continue to get digits

DigitLoop:
	MOV	DX, 0			; Clear DX
	DIV	BX			; Divide n by power for digit
	CMP	AX, 9			; If the digit is < = 9,
	;JLE	LessThanTen		; Input is 0-9
	JG	MoreThanNine		; Otherwise, input is A-F	

LessThanTen:				; 0-9 can be put into memory normally
	ADD	AX, '0'			; '0' is for ASCII			
	MOV	[SI], AX		; Put quotient in memory normally
	JMP	ResetLoop

MoreThanNine:				; Since 10H is represented with A
	SUB	AX, 10			; We need to make 10 the first from A-F
	ADD	AX, 'A'			; 'A' is for ASCII
	MOV	byte ptr[SI], AL	; Input in memory
	;JMP	ResetLoop

ResetLoop:
	MOV	CX, DX			; Put remainder in a holder 
	MOV	AX, BX			; Move power into AX for division
	MOV	BX, 10H	    		; We need to lessen the power, so
	MOV	DX, 0			; Clear DX
	DIV	BX			; Divide power by 10H for next round
	MOV	BX, AX			; Put next divisor in Power
	MOV 	AX, CX			; Put remainder as new n
	INC	SI			; Puts pointer at next memory position
	JMP	HexLoopPowerChk		; Goes to Check if done		

Hex2StringEnd: 				; Once Done
	MOV	byte ptr[SI], ASCII_ZERO; Null Terminate the String
	RET

Hex2String	ENDP



CODE    ENDS



        END
