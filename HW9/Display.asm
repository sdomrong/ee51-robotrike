NAME    Display

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Display                                 ;
;                               Display Functions                            ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; DisplayInit:          Initializes the the two buffers, the counter, and clear
;                       any LED segments that might be on.
; Display:              Converts ASCII represented string to segments and
;                       outputs to an array which is displayed
; DisplayNum:           Displays a 16 bit signed value
; DisplayHex:           Displays a 16 bit unsigned value   
; DisplayMult:          Outputs a character in a LED block when an interrupt is 
;                       received.  
;
; Revision History:
;       11/08/13   Sith Domrongkitchaiporn   Initial Revision
;       11/09/13   Sith Domrongkitchaiporn   Update DisplayInit and Display
;       11/13/13   Sith Domrongkitchaiporn   Update comments
;       12/15/13   Sith Domrongkitchaiporn   Save Register Value in DisplayMult 
;      
CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DATA

EXTRN   Dec2String:NEAR
EXTRN   Hex2String:NEAR
EXTRN   ASCIISegTable:BYTE

$INCLUDE (Display.inc)

;DisplayInit
; Display
;
; Description:          This function initializes the counter and the arrays 
;                       that are going to be use for displays.
;
; Operation:            This function sets the counter to 0 as the beginning.  
;                       It then initializes the arrays by putting 0s in until 
;                       all the arrays are filled.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     counter, segbuffer, decbuffer, hexbuffer, read/write
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:	        None
;
; Registers Changed:    DX, AX
; Stack Depth:          1 word
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/13/13


DisplayInit     PROC        NEAR
                PUBLIC      DisplayInit

DisplayInitInit:
        PUSH    BX
        MOV     counter, 0
        MOV     BX, 0                           ;Set count = 0
        MOV     DX, 8                           ;Writing to port 8 which is not
        MOV     AX, 0                           ;a display, but to remove strange
        OUT     DX, AL                          ;segments from lighting up

DisplayInitLoop:
        CMP     BL, SBUFFERLEN                  ;While counter < array length     
        ;JB      DisplayInitZero                ;Continue filling array
        JAE     DisplayInitDone                 ;Otherwise, we're done

DisplayInitZero:
        MOV     segbuffer[BX], 0                ;Set the array values to 0      
        MOV     strbuffer[BX], 0                ;for initializing        
        INC     BL
        JMP     DisplayInitLoop                 ;Go back to check

DisplayInitDone:
        POP     BX
        RET

DisplayInit     ENDP


; Display
;
; Description:          The function is passed a <null> terminated string (str) 
;                       to output to the LED display.  The string is passed by 
;                       reference in ES:SI.  Since ES is used, the string could 
;                       be in the code segment.  The function figures out what 
;                       to display for each character by looking up the display 
;                       segment table.  The LED block will use a 7 segment 
;                       display.  The text will be left justified.  If the 
;                       string is longer than the number of LEDs, the LEDs will
;                       only display the first 8.  If there's less, the rest of
;                       the blocks will be blank.
;
; Operation:            This function converts the passed in string (str) to
;                       be able to output to the LEDs.  The string has an ASCII 
;                       representation and this function will look up the
;                       display segment table and store it in an array.  If the
;                       string is longer than 8 characters, the array will only 
;                       contain the first 8.  If it is less than 8, the string 
;                       will be left justified and the rest of the array will be
;                       empty.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Display arrays
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes pointer passed is valid.  Can only show 8 digits
;
; Known Bugs:		None
;
; Registers Changed:    None
; Stack Depth:          3 words
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/13/13


Display         PROC        NEAR
                PUBLIC      Display

DisplayLoopInit:
        PUSH    AX                              ;Prevent Register Trashing
        PUSH    BX
        PUSH    CX
        MOV     CX, 0                           ;Initialize length counter

DisplayLoop:
        CMP     CX, SBUFFERLEN                  ;While counter < array length
        ;JB      DisplayBufferInc                ;Continue filling array
        JAE     DisplayLoopDone                 ;Otherwise, we're done

DisplayBufferInc:
        MOV     BX, CX
        MOV     AL, ES:[BX+SI]                  ;Put ASCII value in AL for XLAT        
        CMP     AL, ASCII_NULL                  ;If Str has reach the end,      
        JE      DisplayNull                     ;Fill spaces
        LEA     BX, ASCIISegTable               ;Set Offset of ASCIISegTable
        XLAT    ASCIISegTable                   ;Look up in segment table for
                                                ;ASCII value
        MOV     BX, CX                          ;BL is used for indexing
        MOV     segbuffer[BX], AL               ;Put segment value into array
        INC     CL
        JMP     DisplayLoop                     ;Loop to check if array is full    

DisplayNull:
        CMP     CX, SBUFFERLEN                  ;While counter < array length
        ;JB      DisplayNullLoop                ;Add spaces
        JAE     DisplayLoopDone                 ;Otherwise, we're done

DisplayNullLoop:
        MOV     BX, CX                          ;BX is used for indexing                          
        MOV     segbuffer[BX], SEG_SPACE        ;fill array with spaces
        INC     CX
        JMP     DisplayNull 

DisplayLoopDone:
        POP     CX                              ;Put back old registers value
        POP     BX
        POP     AX
        RET

Display 	ENDP


; DisplayNum
;
; Description:          The function is passed a 16-bit signed value (n) to 
;                       output in decimal (Exactly 5 digits plus sign) to the 
;                       LED display. The number (n) is passed in AX by value.  
;                       The sign will be the at the front of the string and 
;                       the text will be left justified.  The rest of the
;                       display will be blank.          
;
; Operation:            This function calls the Dec2String function to convert
;                       the passed in 16-bit signed value (n) into it's ASCII
;                       representation.  Once this is done, this function will
;                       call display to convert the ASCII representations to 
;                       display segments.
;
; Arguments:            n (AX) - 16 bit signed value
;
; Return Value:         None
; Local Variables:      array address
; Shared Variables:     Array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes pointer passed is valid.  Can only show 8 digits
;
; Known Bugs:           None
;
; Registers Changed:    ES
; Stack Depth:          3 words
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/13/13


DisplayNum      PROC        NEAR
                PUBLIC      DisplayNum

DisplayNumDo:
        PUSH    SI                              ;Prevent register trashing
        LEA     SI, strbuffer                   ;Use strbuffer for Dec2String
        PUSH    SI
        Call    Dec2String                      ;Call conversion function
        POP     SI                              
        PUSH    DS                              ;Put value of DS into ES since
        POP     ES                              ;Display takes the adddress ES
        Call    Display
        POP     SI                              ;Get back old register value
        RET

DisplayNum	ENDP

;-----------------

; DisplayHex
;
; Description:          The function is passed a 16-bit value (n) to output in 
;                       hexadecimal (exactly 4 digits) to the LED display. The 
;                       number (n) is passed in AX by value.  The text will be
;                       left justified.  The rest of the LEd will be blanks.          
;
; Operation:            This function calls the Dec2String function to convert
;                       the passed in 16-bit signed value (n) into it's ASCII
;                       representation.  Once this is done, this function will
;                       call display to convert the ASCII representations to 
;                       display segments.
;
; Arguments:            n (AX) - 16 bit unsigned value
;
; Return Value:         None
; Local Variables:      array address
; Shared Variables:     array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Assumes pointer passed is valid.  Can only show 8 digits
;
; Known Bugs:           None
;
; Registers Changed:    ES
; Stack Depth:          3 words
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        11/13/13


DisplayHex      PROC        NEAR
                PUBLIC      DisplayHex

DisplayHexDo:
        PUSH    SI                              ;Prevent regiser trashing
        LEA     SI, strbuffer
        PUSH    SI
        Call    Hex2String                      ;Call conversion function
        POP     SI
        PUSH    DS
        POP     ES
        Call    Display                         
        POP     SI                              ;Get old register value
        RET

DisplayHex	ENDP

;---------------------

; DisplayMult
;
; Description:          The function displays a character on the correct digit
;                       of the LED block.  This function will rely on a timer
;                       register to call it.  It determines the correct digit to
;                       show by looking up the variable counter.        
;
; Operation:            Every time the timer reaches max count.  This function
;                       will be called to increment the counter and display the
;                       the correct character on the correct LED block.  The
;                       counter is used to determine where the character will be
;                       and on the LED block and what to display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               A digit on the LED
; 
; Error Handling:       None
;
; Algorithms:           Array wrapping.  counter + 1 AND length - 1 
;                       We assume 2^n length.
;
; Data Structures:      None
;
; Limitations:          Can only show 8 digits
;
; Known Bugs:           None
;
; Registers Changed:    None
; Stack Depth:          None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/15/13

DisplayMult     PROC    NEAR
                PUBLIC  DisplayMult

DisplayMultInit:
        PUSH    AX                              ;Prevent register trashing
        PUSH    BX
        PUSH    DX                
        LEA     BX, segbuffer                   ;Use segbuffer as start address
        ADD     BL, counter

DisplayMultLED:
        MOV     DX, DISPLAYPORT                 ;Outputting to display port
        ADD     DL, counter
        MOV     AX, [BX]
        
        OUT     DX, AL

EndDisplayMult:                                 ;Array wrapping algorithm
        INC     counter
        AND     counter, SBUFFERLEN-1           ;We are using 2^n array length
        POP     DX                              ;Get back old register value
        POP     BX
        POP     AX
        RET
    
DisplayMult   ENDP

CODE    ENDS

; Data
DATA    SEGMENT PUBLIC  'DATA'

counter         DB    ?               		;Points to position of array
segbuffer       DB SBUFFERLEN DUP (?)           ;Array for strings for display
strbuffer       DB SBUFFERLEN DUP (?)           ;Array for Dec2String/Hex2String

DATA ENDS


END
