NAME    MainLoop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Main Loop                                ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; These are functions that the remote side of the mainloop uses.  Mainly, it
; handles getting the events and decide what to do with the events.  There are
; two types of events that could happen: Keypad event and Error event.
;
;       ErrorTable:     Table of offsets for serial errors.  There are 5 types
;                       of errors possible: Break, Buffer Overflow, Framing,
;                       Parity, and Overrun.  Each of these functions display
;                       the message on the LED display.
;       EnqueueEvent:   Enqueues the Keyevent and Keyvalue
;       DequeueEvent:   Dequeues the keyevent and keyvalue and decide what 
;                       action to take.  There are 2 possible events, error or
;                       keypad.
;       MainInit:       Initializes the shared variables, buffers and queues 
;                       necessary for Main Functions.
;                      
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision
;       12/17/13   Sith Domrongkitchaiporn   Update Comments and Descriptions

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP

        
EXTRN   KeypadTable:WORD       
EXTRN   QueueFull:NEAR
EXTRN   QueueEmpty:NEAR
EXTRN   Dequeue:NEAR
EXTRN   Enqueue:NEAR
EXTRN   QueueInit:NEAR  
EXTRN   Display:NEAR
        
$INCLUDE (MainLoop.inc)        
        
        
; ErrorTable 
;
; Description:          This is a table of offsets that could be called for 
;                       Serial Error.        
;
; Notes:                Function offsets are words.
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13
        
ErrorTable      Label   WORD

        DW      OFFSET(BufferError)
        DW      OFFSET(OverrunError)
        DW      OFFSET(ParityError)
        DW      OFFSET(FramingError)
        DW      OFFSET(BreakError)


; EnqueueEvent
;
; Description:          This function utilizes an event queue.  It enqueues the
;                       the key type and key value into a queue.  The key type 
;                       is passed in as AH and the key value is passed as in 
;                       AL.
;
; Operation:            Enqueue the keyevent and keyvalues into an eventqueue.
;
; Arguments:            KeyEvent (AH) and KeyValue (AL)
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       Saves serial errors
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          Assume correct values for Keyevent and Keyvalue
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

EnqueueEvent    PROC        NEAR
                PUBLIC      EnqueueEvent
        
        PUSH    SI                      ;Save registers
        LEA     SI, event               ;Get the offset for event buffer
        CALL    QueueFull               ;If queue is full,
        JZ      EnqueueEventFull        ;there's a critical error.
        CALL    Enqueue                 ;Otherwise, enqueue it.
        JMP     EnqueueEventDone        ;We're done
        
EnqueueEventFull:                       ;There's a BIG problem
        CALL    MainInit                ;Reset Hard

EnqueueEventDone:
        POP     SI                      ;Restore old register value
        RET
        
EnqueueEvent    ENDP


; DequeueEvent
;
; Description:          This function dequeues the eventqueue.  The first value
;                       is the keytype and the second value is the keyvalue. The
;                       function then decides what to call. 
;
; Operation:            Dequeue the keyevent and keyvalues.  Calls the right 
;                       functions to deal with the event from a table.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          Deals with only Serial and Keypad Events.
;
; Known Bugs:           None
;
; Registers Changed:    AX
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

DequeueEvent    PROC        NEAR
                PUBLIC      DequeueEvent
        
DequeueEventInit:        
        PUSH    AX                              ;Save old register values
        PUSH    BX
        LEA     SI, event                       ;Get the offset for event buffer
        CALL    QueueEmpty                      ;Check if Queue is empty
        JZ      DequeueEventDone                ;If yes, do nothing.
        CALL    Dequeue                         ;Otherwise, dequeue the event
        CMP     AH, ErrorEvent                  ;Check if it's a serial error
        JE      DequeueEventError               ;If yes then deal with it.
        JNE     DequeueEventKeypad              ;Otherwise, it's a keypad event
        
DequeueEventError:
        MOV     BX, 0                           ;Clear BX
        MOV     BL, AL                          ;Move the key value into BL
        SHL     BX, 1                           ;Prepare to Call functions
        CALL    CS:ErrorTable[BX]               
        JMP     DequeueEventDone                ;We're done
        
DequeueEventKeypad:
        MOV     BX, 0                           ;Clear BX
        MOV     BL, AL                          ;Move Keyvalue into BL
        SHL     BX, 1                           ;Prepare to call from table
        CALL    CS:KeypadTable[BX]
        JMP     DequeueEventDone

DequeueEventDone:
        POP     BX                              ;Save register values
        POP     AX
        RET
        
DequeueEvent    ENDP


; MainInit
;
; Description:          This function initializes the queues and variables for
;                       the usage of the main loop.  This maybe called if the 
;                       main loop reaches a critical error and requires a reset
;                       hard.
;
; Operation:            Initialize the event queue by calling QueueInit.  It
;                       initializes the display buffer by filling the buffer
;                       with 0s.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

MainInit        PROC        NEAR
                PUBLIC      MainInit
        
        PUSH    SI                              ;Save register values
        PUSH    BX
        
MainInitInit:
        LEA     SI, event                       ;Get address of queue
        MOV     BX, 1                           ;This is a WORD queue
        CALL    QueueInit                       
        MOV     SI, 0                           ;Clear SI
        
MainInitDisplayClr:                             ;SI is used as counter
        CMP     SI, DISPBLOCK                   ;If counter >= DISPBLOCK
        JGE     MainInitDone                    ;We're done
        MOV     dispbuf[SI], 0                  ;Otherwise, put 0 in.
        INC     SI              
        JMP     MainInitDisplayClr

MainInitDone:        
        POP     BX                              ;Restore old register values
        POP     SI
        RET
MainInit        ENDP


; OverrunError
;
; Description:          This function displays "OVR ERR".  If the serial 
;                       receives an Overrun Error.
;
; Operation:            Puts each character of "OVR ERR" into the display buffer
;                       and calls display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Can only display 8 characters
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

OverrunError    PROC        NEAR
                PUBLIC      OverrunError
        
        PUSH    SI                              ;Save Register values
        PUSH    ES
        MOV     dispbuf[0], 'O'                 ;Puts ASCII characters into 
        MOV     dispbuf[1], 'V'                 ;display buffer
        MOV     dispbuf[2], 'R'
        MOV     dispbuf[3], ' '
        MOV     dispbuf[4], 'E'
        MOV     dispbuf[5], 'R'
        MOV     dispbuf[6], 'R'
        LEA     SI, dispbuf                     ;Get the offset for dispbuf
        PUSH    DS                              ;Puts value of DS into ES
        POP     ES                              ;since Display function uses
                                                ;ES
        CALL    Display                         
        POP     ES                              ;Restore Old register values
        POP     SI        
        RET       

OverrunError    ENDP


; ParityError
;
; Description:          This function displays "PAR ERR".  If the serial 
;                       receives a Parity Error.
;
; Operation:            Puts each character of "PAR ERR" into the display buffer
;                       and calls display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Can only display 8 characters
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

ParityError     PROC        NEAR
                PUBLIC      ParityError
        
        PUSH    SI                              ;Save old regiser Values
        PUSH    ES
        MOV     dispbuf[0], 'P'                 ;Puts ASCII characters in 
        MOV     dispbuf[1], 'A'                 ;display buffer
        MOV     dispbuf[2], 'R'
        MOV     dispbuf[3], ' '
        MOV     dispbuf[4], 'E'
        MOV     dispbuf[5], 'R'
        MOV     dispbuf[6], 'R'
        LEA     SI, dispbuf                     ;Get offset for display buffer
        PUSH    DS                              ;Display function uses ES 
        POP     ES                              ;(ES overide)
        CALL    Display
        POP     ES                              ;Restore register values
        POP     SI        
        RET       

ParityError     ENDP


; FramingError
;
; Description:          This function displays "FRM ERR".  If the serial 
;                       receives a Framing Error.
;
; Operation:            Puts each character of "FRM ERR" into the display buffer
;                       and calls display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Can only display 8 characters
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

FramingError    PROC        NEAR
                PUBLIC      FramingError
        
        PUSH    SI                              ;Save register values
        PUSH    ES      
        MOV     dispbuf[0], 'F'                 ;Puts ASCII Characters into
        MOV     dispbuf[1], 'R'                 ;display buffer
        MOV     dispbuf[2], 'M'
        MOV     dispbuf[3], ' '
        MOV     dispbuf[4], 'E'
        MOV     dispbuf[5], 'R'
        MOV     dispbuf[6], 'R'
        LEA     SI, dispbuf                     ;Gets the offset of dispbuf
        PUSH    DS                              ;Display function uses ES
        POP     ES                              ;Overide
        CALL    Display
        POP     ES                              ;Restore old register value
        POP     SI        
        RET       

FramingError    ENDP


; BreakError
;
; Description:          This function displays "BRK ERR".  If the serial 
;                       receives a Breaking Error.
;
; Operation:            Puts each character of "BRK ERR" into the display buffer
;                       and calls display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Only has 8 display blocks
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

BreakError      PROC        NEAR
                PUBLIC      BreakError
        
        PUSH    SI                              ;Save Register values
        PUSH    ES              
        MOV     dispbuf[0], 'B'                 ;Puts ASCII characters into
        MOV     dispbuf[1], 'R'                 ;Display Buffer
        MOV     dispbuf[2], 'K'
        MOV     dispbuf[3], ' '
        MOV     dispbuf[4], 'E'
        MOV     dispbuf[5], 'R'
        MOV     dispbuf[6], 'R'
        LEA     SI, dispbuf                     ;Get display buffer offset
        PUSH    DS                              ;Display function uses ES 
        POP     ES                              ;Overide
        CALL    Display
        POP     ES                              ;Restore old register values
        POP     SI        
        RET       

BreakError     ENDP


; BufferError           - NOT YET IMPLEMENTED
;
; Description:          This function displays "BUF ERR".  If the serial 
;                       receives a Buffer Overflow Error.
;
; Operation:            Puts each character of "BUF ERR" into the display buffer
;                       and calls display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     None
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          Can only display 8 chracters at a time
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

BufferError     PROC        NEAR
                PUBLIC      BufferError
        
        PUSH    SI                              ;Save Register Values
        PUSH    ES
        MOV     dispbuf[0], 'B'                 ;Puts ASCII characters into
        MOV     dispbuf[1], 'U'                 ;display buffer
        MOV     dispbuf[2], 'F'
        MOV     dispbuf[3], ' '
        MOV     dispbuf[4], 'E'
        MOV     dispbuf[5], 'R'
        MOV     dispbuf[6], 'R'
        LEA     SI, dispbuf                     ;Get offset for display buffer
        PUSH    DS                              ;Display function uses ES 
        POP     ES                              ;Overide
        CALL    Display
        POP     ES                              ;Restore old register values
        POP     SI        
        RET       

BufferError     ENDP

CODE    ENDS

; Data

DATA    SEGMENT PUBLIC  'DATA'

event           Q_EVENT <>                       ;Initializing queue 
dispbuf         DB  (8) DUP (?)                  ;Array for storing display

DATA ENDS

END
