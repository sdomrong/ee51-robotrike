;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Main Loop                                ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Descriptions and ToC
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision
;       12/17/13   Sith Domrongkitchaiporn   Update comments

MAX_BYTE_QUEUE  EQU     256                 ; Max size for Byte Queue 
ERROREVENT      EQU     2                   ; Error Key Type
DISPBLOCK       EQU     8                   ; No. of Display blocks        

Q_EVENT        STRUC                        ; Queue structure
    head        DW      ?                   ; Head position of Queue
    tail        DW      ?                   ; Tail position of the Queue
    len         DW      ?                   ; Length of Queue
    siz         DB      ?                   ; Size of an element in the queue
    array       DB (MAX_BYTE_QUEUE+1) DUP (?) ; Array used to store the queue
Q_EVENT        ENDS
