;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Queue                                   ;
;                               Queue Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Revision History:
;    11/01/13  Sith Domrongkitchaiporn	initial version
;    11/02/13  Sith Domrongkitchaiporn	Fix Syntax
;
; Constants
MAX_BYTE_QUEUE  EQU     256                 ; Max size for Byte Queue          
MAX_WORD_QUEUE  EQU     128                 ; Max size for Word Queue        
INIT_PTR        EQU     0                   ; Pointer = 0
INIT_LEN        EQU     1                   ; Initial length = 1
BYTE_SIZE       EQU     1                   ; Size = 1 for BYTE
WORD_SIZE       EQU     2                   ; Size = 2 for WORD

Q_STRUCT        STRUC                       ; Queue structure
    head        DW      ?                   ; Head position of Queue
    tail        DW      ?                   ; Tail position of the Queue
    len         DW      ?                   ; Length of Queue
    siz         DB      ?                   ; Size of an element in the queue
    array       DB (MAX_BYTE_QUEUE+1) DUP (?) ; Array used to store the queue
Q_STRUCT        ENDS
