NAME  Eventh

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   MainLoop                                 ;
;                               Main EventHandler                            ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; These functions are used by the eventhandlers for the remote side of the main
; loop.
;       
; TimerEventHandler:    Calls DisplayMult when a timer interrupt is received to
;                       output LED.  Calls KeypadScan to see which key is 
;                       pressed.  Outputs the timer EOI
; SerialEvent:          Calls SerialHandler and returns with INT2 EOI.
; InitCS:               Initialize the chip select so the LED can be output 
; InitTimer:            Initializes the value of the timer registers including
;                       max count.
; InitInt2:             Initializes the handling of INT2 interrupts
; InstallHandler:       Install the event handlers into the interrupt vector
;                       table                    
; ClrIRQVectors:        Clear any initial handlers in the vector table.
; IllegalEventHandler:  Any strange interrupts will go here.
;
; Revision History:
;       11/08/13   Sith Domrongkitchaiporn   Initial Revision
;       11/16/13   Sith Domrongkitchaiporn   Use only one timer
;                                            Update for keypad
;       11/17/13   Sith Domrongkitchaiporn   Update Comments
;
; local include files
$INCLUDE(Display.inc)
$INCLUDE(Int188.inc)
$INCLUDE(Timer188.inc)

CGROUP  GROUP   CODE

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP

EXTRN   DisplayMult:NEAR
EXTRN   KeypadScan:NEAR
EXTRN   SerialHandler:NEAR        

; TimerEventHandler
;
; Description:       This procedure is the event handler for the timer
;                    interrupt.  It outputs the next segment pattern to the
;                    LED display.  After going through all the segment
;                    patterns for a digit it goes on to the next digit.  After
;                    doing all the digits it starts over again.
;
; Operation:         Output the segment pattern to the LED then update the
;                    segment pattern index.  If at the end of the segment
;                    patterns, update the LED number.  If at the end of the
;                    LEDs, wrap back to the first one.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None
; Shared Variables:  None.
; Global Variables:  None
;
; Input:             None.
; Output:            None
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       3 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/09/13

TimerEventHandler       PROC    NEAR
                        PUBLIC  TimerEventHandler

TimerEventHandlerInit:
        PUSH    AX                      ;save the registers
        PUSH    DX                      

DisplayUpdate:                        
        Call    DisplayMult             ;Call the function for display
        CALL    KeypadScan

EndTimerEventHandler:                   ;done taking care of the timer
        MOV     DX, EOI                 ;send EOI to the interrupt controller
        MOV     AX, TIMEREOI
        OUT     DX, AX

TimerEventHandlerEnd:
        POP     DX                      ;restore the registers
        POP     AX
        IRET                            ;Event Handlers end with IRET not RET

TimerEventHandler       ENDP

; SerialEvent
;
; Description:       This procedure is the event handler for the serial
;                    interrupt.  It checks to see if there is anything the
;                    the serial has to do.
;
; Operation:         This function calls the serial event handler and outputs
;                    the EOI for INT2.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None
; Shared Variables:  None.
; Global Variables:  None
;
; Input:             None.
; Output:            None
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/23/13

SerialEvent             PROC    NEAR
                        PUBLIC  SerialEvent

SerialEventHandlerInit:
        PUSH    AX                      ;save the registers
        PUSH    DX                      

CALLSerialHandler:                        
        CALL    SerialHandler           ;Call the function for Serial

EOISerialEvent:                         
        MOV     DX, EOI                 ;send EOI to the interrupt controller
        MOV     AX, INT2EOI
        OUT     DX, AX

SerialEventEnd:
        POP     DX                      ;restore the registers
        POP     AX
        IRET                            ;Event Handlers end with IRET not RET

SerialEvent ENDP


; InitCS
;
; Description:       Initialize the Peripheral Chip Selects on the 80188.
;
; Operation:         Write the initial values to the PACS and MPCS registers.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/09/13

InitCS  PROC    NEAR
        PUBLIC  InitCS
        
        PUSH    AX
        PUSH    DX
        MOV     DX, PACSreg     ;setup to write to PACS register
        MOV     AX, PACSval
        OUT     DX, AX          ;write PACSval to PACS

        MOV     DX, MPCSreg     ;setup to write to MPCS register
        MOV     AX, MPCSval
        OUT     DX, AX          ;write MPCSval to MPCS
        POP     DX
        POP     AX
        RET                     ;done so return


InitCS  ENDP


; InitTimer
;
; Description:       Initialize the 80188 Timers.  The timers are initialized
;                    to generate interrupts.  The interrupt controller is also 
;                    initialized to allow the timer interrupts.  Timer 2 is the
;                    one being used.
;
; Operation:         The appropriate values are written to the timer control
;                    registers in the PCB.  Also, the timer count registers
;                    are reset to zero.  Finally, the interrupt controller is
;                    setup to accept timer interrupts and any pending
;                    interrupts are cleared by sending a TimerEOI to the
;                    interrupt controller.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       None
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/09/13

InitTimer       PROC    NEAR
                PUBLIC  InitTimer
                
        PUSH    AX
        PUSH    DX
                                ;initialize Timer #2
        MOV     DX, T2CNTADR    ;initialize the count register to 0
        XOR     AX, AX
        OUT     DX, AX

        MOV     DX, T2CMPAADR   ;setup max count
        MOV     AX, T2CMPAVAL
        OUT     DX, AX

        MOV     DX, T2CONADR    ;setup the control register, with interrupts
        MOV     AX, TMRCON2EN OR TMRCON2INH OR TMRCON2CONT     ;Value for Timer2
        OR      AX, TMRCON2INT
        OUT     DX, AX
                                ;initialize interrupt controller for timers
        MOV     DX, TMRCON      ;setup the interrupt control register
        MOV     AX, TMRCONVAL
        OUT     DX, AX

        MOV     DX, EOI         ;send a timer EOI (to clear out controller)
        MOV     AX, TIMEREOI
        OUT     DX, AX
        POP     DX
        POP     AX
        RET                     ;done so return


InitTimer       ENDP

; InitInt2
;
; Description:       Initialize Int2 for use with Serial.  
;
; Operation:         Initializes INT 2 for serial use.  It is initialize as INT2
;                    having an edge trigger and priority level 1.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       None
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/30/13

InitInt2        PROC    NEAR
                PUBLIC  InitInt2
                
        PUSH    AX
        PUSH    DX
        
        XOR     AX, AX
        
        MOV     AX, INTNCPR1    ;Write appropriate value for INT2
        MOV     DX, INT2CON
        OUT     DX, AX
        
        MOV     DX, EOI         ;send a INT2 EOI
        MOV     AX, INT2EOI
        OUT     DX, AX
        
        POP     DX
        POP     AX
        RET                     ;done so return


InitInt2        ENDP


; InstallHandler
;
; Description:       Install the event handler for the timer interrupt.
;
; Operation:         Writes the address of the timer event handler to the
;                    appropriate interrupt vector.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: flags
; Stack Depth:       0 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/09/13

InstallHandler  PROC    NEAR
                PUBLIC  InstallHandler
                
        PUSH    AX
        PUSH    ES
        XOR     AX, AX          ;clear ES (interrupt vectors are in segment 0)
        MOV     ES, AX
                                ;store the vector
        MOV     ES: WORD PTR (4 * Tmr2Vec), OFFSET(TimerEventHandler)
        MOV     ES: WORD PTR (4 * Tmr2Vec + 2), SEG(TimerEventHandler)
        MOV     ES: WORD PTR (4 * Int2Vec), OFFSET(SerialEvent)
        MOV     ES: WORD PTR (4 * Int2Vec + 2), SEG(SerialEvent)
        POP     ES
        POP     AX
        RET                     ;all done, return


InstallHandler  ENDP


; ClrIRQVectors
;
; Description:      This function installs the IllegalEventHandler for all
;                   interrupt vectors in the interrupt vector table.  Note
;                   that all 256 vectors are initialized so the code must be
;                   located above 400H.  The initialization skips  (does not
;                   initialize vectors) from vectors FIRST_RESERVED_VEC to
;                   LAST_RESERVED_VEC.
;
; Arguments:        None.
; Return Value:     None.
;
; Local Variables:  CX    - vector counter.
;                   ES:SI - pointer to vector table.
; Shared Variables: None.
; Global Variables: None.
;
; Input:            None.
; Output:           None.
;
; Error Handling:   None.
;
; Algorithms:       None.
; Data Structures:  None.
;
; Registers Used:   flags
; Stack Depth:      1 word
;
; Author:           Sith Domrongkitchaiporn
; Last Modified:    11/09/13

ClrIRQVectors   PROC    NEAR
                PUBLIC  ClrIRQVectors

InitClrVectorLoop:              ;setup to store the same handler 256 times
        PUSH    AX
        PUSH    CX
        PUSH    SI
        PUSH    ES
        XOR     AX, AX          ;clear ES (interrupt vectors are in segment 0)
        MOV     ES, AX
        MOV     SI, 0           ;initialize SI to skip Reserved interrupts

        MOV     CX, 256         ;up to 256 vectors to initialize


ClrVectorLoop:                  ;loop clearing each vector
                                ;check if should store the vector
        CMP     SI, 4 * FIRST_RESERVED_VEC
        JB      DoStore	        ;if before start of reserved field - store it
        CMP     SI, 4 * LAST_RESERVED_VEC
        JBE     DoneStore       ;if in the reserved vectors - don't store it
        ;JA     DoStore		;otherwise past them - so do the store

DoStore:                            ;store the vector
        MOV     ES: WORD PTR [SI], OFFSET(IllegalEventHandler)
        MOV     ES: WORD PTR [SI + 2], SEG(IllegalEventHandler)

DoneStore:                          ;done storing the vector
        ADD     SI, 4               ;update pointer to next vector

        LOOP    ClrVectorLoop       ;loop until have cleared all vectors
        ;JMP    EndClrIRQVectors    ;and all done


EndClrIRQVectors:                   ;all done, return
        POP     ES
        POP     SI
        POP     CX
        POP     AX
        RET


ClrIRQVectors   ENDP




; IllegalEventHandler
;
; Description:       This procedure is the event handler for illegal
;                    (uninitialized) interrupts.  It does nothing - it just
;                    returns after sending a non-specific EOI.
;
; Operation:         Send a non-specific EOI and return.
;
; Arguments:         None.
; Return Value:      None.
;
; Local Variables:   None.
; Shared Variables:  None.
; Global Variables:  None.
;
; Input:             None.
; Output:            None.
;
; Error Handling:    None.
;
; Algorithms:        None.
; Data Structures:   None.
;
; Registers Changed: None
; Stack Depth:       2 words
;
; Author:            Sith Domrongkitchaiporn
; Last Modified:     11/13/13

IllegalEventHandler     PROC    NEAR
                        PUBLIC  IllegalEventHandler

        NOP                             ;do nothing (can set breakpoint here)

        PUSH    AX                      ;save the registers
        PUSH    DX

        MOV     DX, EOI                 ;send a non-specific EOI to the
        MOV     AX, EOINSPEC            ;interrupt controller to clear out
        OUT     DX, AX                  ;the interrupt that got us here

        POP     DX                      ;restore the registers
        POP     AX

        IRET                            ;and return


IllegalEventHandler     ENDP

CODE ENDS

END
