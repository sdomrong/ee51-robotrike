;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Serial                                  ;
;                               Serial Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
; Include file for serial functions
;
; Revision History:
;    11/29/13  Sith Domrongkitchaiporn	initial version


;Constants
NOPARITY        EQU     0               ;No Parity Option 
ODDPARITY       EQU     1               ;Odd Parity Option
EVENPARITY      EQU     2               ;Even Parity Option
STICKLOW        EQU     3               ;Stick Low Parity Option
STICKHIGH       EQU     4               ;Stick High Parity Option
        
LSRCHK          EQU     4               ;The value to compare for LSR
CHAREVENT       EQU     1               ;Character Event
ERROREVENT      EQU     2               ;Error Event

;Initialization
INITSIZE        EQU     8               ;Initial Size 
INITSTOP        EQU     1               ;Initial Stop bits
INITPARITY      EQU     0               ;Initial Parity
INITDIV         EQU     60            ;Desire Baud Rate: 9600                


;Addresses
SERIALADR       EQU     100h

;Offset
IIRADR          EQU     SERIALADR + 2
LCRADR          EQU     SERIALADR + 3
LSRADR          EQU     SERIALADR + 5
MSRADR          EQU     SERIALADR + 6
IERADR          EQU     SERIALADR + 1

MSBADR          EQU     SERIALADR + 1
LSBADR          EQU     SERIALADR       ;LSB is the first register
THRADR          EQU     SERIALADR
RBRADR          EQU     SERIALADR

IERINIT         EQU     00000111B
