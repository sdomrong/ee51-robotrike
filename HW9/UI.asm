NAME    UI
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                     UI                                     ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Descriptions and ToC
;
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision
;       12/14/13   Sith Domrongkitchaiporn   Fix Syntax
;       12/15/13   Sith Domrongkitchaiporn   Completely Working Remote side
;       12/17/13   Sith Domrongkitchaiporn   Update Comments and Descriptions

CGROUP  GROUP   CODE
DGROUP  GROUP   DATA

CODE	SEGMENT PUBLIC 'CODE'

        ASSUME  CS:CGROUP, DS:DGROUP

EXTRN   SerialPutChar:NEAR
EXTRN   Dec2String:NEAR
EXTRN   DisplayNum:NEAR
EXTRN   EnqueueEvent:NEAR        
        
$INCLUDE (UI.inc)

; KeypadTable 
;
; Description:          This is a table of offsets that could be called for 
;                       pressing a keyboard.        
;
; Notes:                Function offsets are words.
;
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13
        
KeypadTable     Label   WORD
                PUBLIC  KeypadTable

        DW      OFFSET(ForwardLeft)
        DW      OFFSET(Forward)
        DW      OFFSET(ForwardRight)
        DW      OFFSET(LEDOn)
        DW      OFFSET(Left)
        DW      OFFSET(Stop)
        DW      OFFSET(Right)
        DW      OFFSET(LEDOff)
        DW      OFFSET(DisplaySpeed)
        DW      OFFSET(Backward)
        DW      OFFSET(DisplayAngle)
        DW      OFFSET(KeyNOP)   
        DW      OFFSET(IncSpeed)
        DW      OFFSET(DecSpeed)
        DW      OFFSET(IncAngle)
        DW      OFFSET(DecAngle)




; UIInit
;
; Description:          This function initializes all the shared variables and 
;                       buffers for use.
;
; Operation:            Clears the shared variables, speed, laser and angle.
;                       Clears the buffers, array and display.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Laser, Speed, Angle, array, display
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      None
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

UIInit          PROC        NEAR
                PUBLIC      UIInit

UIInitInit:
        PUSH    BX                              ;Save old register values
        PUSH    SI
        MOV     Speed, 0                        ;Initialize shared variables
        MOV     Laser, 0
        MOV     Angle, 0
        MOV     BX, 0                           ;Clears Register for loop usage
        MOV     SI, 0        
        
UIInitArrayClr:
        CMP     BX, 20                          ;Clears the array 
        JGE     UIInitDisplayClr
        MOV     array[BX], 0
        INC     BX
        JMP     UIInitArrayClr

UIInitDisplayClr:
        CMP     SI, 8                           ;Clears the buffer display
        JGE     UIInitDone
        MOV     display[SI], 0
        INC     SI
        JMP     UIInitDisplayClr

UIInitDone:
        POP     SI                              ;Restore Old register values
        POP     BX
        RET                   

UIInit  ENDP


; ForwardLeft
;
; Description:          This function sends the commands necessary to move the
;                       robotrike forward and 45 degrees to the left.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 315 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, Array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

ForwardLeft     PROC        NEAR
                PUBLIC      ForwardLeft 

ForwardLeftInit:
        PUSH    AX                              ;Save old register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX for usage
        MOV     SI, OFFSET(array)               ;Get the offset for array
       
ForwardLeftClrAngle:
        MOV     AL, 'D'                         ;D is the command for angle
        MOV     [SI], AX                        ;Put it in array
        INC     SI
        MOV     AX, angle                       ;Get current angle
        NEG     AX                              ;Take the -ve of it
        PUSH    SI      
        CALL    Dec2String                      ;Convert number to ASCII and
        POP     SI                              ;puts in array
        ADD     SI, 6                           ;Dec2string ocupies a sign and 5 
                                                ;numbers
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI            

ForwardLeftNewAngle:
        MOV     angle, 315                      ;Our new angle is now 315
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'D'                         ;D is the command for angle
        MOV     [SI], AX                        ;Put in array
        INC     SI
        MOV     AX, angle                       ;Puts new angle in register for
        PUSH    SI                              ;convertion to ASCII
        CALL    Dec2String                      ;Dec2string convertion occupies
        POP     SI                              ;6 spots in array.  Hence,
        ADD     SI, 6                           ;counter needs to increase by 6
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI           

ForwardLeftSpeed:
        MOV     speed, MAXSPEED                 ;Our speed is MAXSPEED
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;S is the speed command
        MOV     [SI], AX                        ;puts in array
        INC     SI      
        MOV     AX, speed                       
        PUSH    SI
        CALL    Dec2String                      ;Converts speed to ASCII using
        POP     SI                              ;Dec2String Function
        ADD     SI, 6                           ;Dec2String occupies 6 spots in 
                                                ;array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET      
        INC     SI
        MOV     BX, OFFSET(array)               ;Move original offset of array
                                                ;into register to use as counter
ForwardLeftSerialLoop:
        CMP     BX, SI                          ;Until we reached the end of 
        JAE     ForwardLeftDone                 ;array, keep grabing value in
        MOV     AL, [BX]                        ;array and call SerialPutChar
        CALL    SERIALPUTCHAR                   ;to send character over with
        INC     BX                              ;serial
        JMP     ForwardLeftSerialLoop

ForwardLeftDone:
        POP     SI                              ;Save old register values
        POP     BX
        POP     AX
        RET                   

ForwardLeft     ENDP


; Forward
;
; Description:          This function sends the commands necessary to move the
;                       robotrike forward.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 0 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, Array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

Forward         PROC        NEAR
                PUBLIC      Forward

ForwardInit:
        PUSH    AX                              ;Save old register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX
        MOV     SI, OFFSET(array)               ;Gets the array offset

ForwardClrAngle:        
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        PUSH    SI                              
        MOV     AX, angle               
        NEG     AX
        CALL    Dec2String                      ;Converts angle to ASCII
        POP     SI                                      
        ADD     SI, 6                           ;Dec2String occupies 6 spots in
                                                ;array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     angle, 0                        ;Store angle as 0

ForwardSpeed:
        MOV     speed, MAXSPEED                 
        XOR     AX, AX                          ;Clear AX            
        MOV     AL, 'S'                         ;'S' is the Abs[Speed] command
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        PUSH    SI
        MOV     AX, speed                       ;Converts speed to ASCII using
        CALL    Dec2String                      ;Dec2string.
        POP     SI                              ;The ASCII values has 6 chracter
        ADD     SI, 6                           ;array + 6.
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset for array
                                                ;Will be used as counter
ForwardSerialLoop:
        CMP     BX, SI                          ;Until we reached the end of                    
        JAE     ForwardDone                     ;array, keep grabing value in
        MOV     AL, [BX]                        ;array and call SerialPutChar
        CALL    SERIALPUTCHAR                   ;to send character over with
        INC     BX                              ;serial
        JMP     ForwardSerialLoop

ForwardDone:
        POP     SI                              ;Restore register values
        POP     BX
        POP     AX
        RET                   

Forward         ENDP


; ForwardRight
;
; Description:          This function sends the commands necessary to move the
;                       robotrike forward and 45 degrees to the right.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 45 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, Array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

ForwardRight    PROC        NEAR
                PUBLIC      ForwardRight 

ForwardRightInit:
        PUSH    AX                              ;Save register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clear AX
        MOV     SI, OFFSET(array)               ;Gets array offset
       
ForwardRightClrAngle:
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        MOV     AX, angle                       ;Gets current angle
        NEG     AX                              ;Take the -ve value
        PUSH    SI
        CALL    Dec2String                      ;Converts angle to ASCII
        POP     SI                              ;ASCII conversion takes up 6 
        ADD     SI, 6                           ;spots in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI            

ForwardRightNewAngle:
        MOV     angle, 45                       ;New angle is 45 degrees
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        MOV     AX, angle                       ;Set a new angle
        PUSH    SI                              ;Convert angle to ASCII with
        CALL    Dec2String                      ;Dec2String.
        POP     SI                              ;Conversion uses 6 spots in 
        ADD     SI, 6                           ;array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI           

ForwardRightSpeed:
        MOV     speed, MAXSPEED                 ;Set speed to MAXSPEED
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;'S' is for Abs[Speed]
        MOV     [SI], AX                        ;Store command in array
        INC     SI      
        MOV     AX, speed                       
        PUSH    SI                              ;Convert new speed to ASCII
        CALL    Dec2String                      ;using dec2string
        POP     SI                              ;Dec2String uses 6 spots in the
        ADD     SI, 6                           ;array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset and use it
                                                ;as a counter
ForwardRightSerialLoop:
        CMP     BX, SI                          ;Send all store chracters over
        JAE     ForwardRightDone                ;serial, one by one with 
        MOV     AL, [BX]                        ;SerialPutChar
        CALL    SERIALPUTCHAR
        INC     BX
        JMP     ForwardRightSerialLoop

ForwardRightDone:
        POP     SI                              ;Restore register value
        POP     BX
        POP     AX
        RET                   

ForwardRight    ENDP

; Left
;
; Description:          This function sends the commands necessary to move the
;                       robotrike left with full speed.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 270 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, Array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

Left            PROC        NEAR
                PUBLIC      Left 

LeftInit:
        PUSH    AX                              ;Restore register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX
        MOV     SI, OFFSET(array)               ;Get offset of array
       
LeftClrAngle:
        MOV     AL, 'D'                         ;'D' is the command for angles
        MOV     [SI], AX                        ;store in array
        INC     SI      
        MOV     AX, angle                       ;Get current angle
        NEG     AX                              ;Take the -ve value
        PUSH    SI                              ;Convert to Characters using
        CALL    Dec2String                      ;Dec2string.
        POP     SI                              ;The character fills up 6 spots
        ADD     SI, 6                           ;in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI            

LeftNewAngle:
        MOV     angle, 270                      ;Moving left 270 degrees              
        XOR     AX, AX                          ;Clear AX
        MOV     AL, 'D'                         ;'D' is the angle command
        MOV     [SI], AX                        ;store command in array
        INC     SI
        MOV     AX, angle                       ;Convert new angle into ASCII
        PUSH    SI                              ;using Dec2String.  
        CALL    Dec2String                      ;the character value occupies 6
        POP     SI                              ;spots in array.  Hence, our
        ADD     SI, 6                           ;counters is +6
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI           

LeftSpeed:
        MOV     speed, MAXSPEED                 ;New Speed is now MAXSPEED
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;'S' is Abs[speed] command
        MOV     [SI], AX                        ;Store command in array
        INC     SI                      
        MOV     AX, speed
        PUSH    SI                              ;Convert number to ASCII for
        CALL    Dec2String                      ;sending over serial
        POP     SI                              ;The conversion takes up 6
        ADD     SI, 6                           ;spots in the array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get the initial offset of array
                                                ;for use as counter
LeftSerialLoop:
        CMP     BX, SI                          ;Keep grabing a character in the
        JAE     LeftDone                        ;array and send it one by one
        MOV     AL, [BX]                        ;over serial by calling 
        CALL    SERIALPUTCHAR                   ;SerialPutChar until all the 
        INC     BX                              ;chracters have been sent
        JMP     LeftSerialLoop

LeftDone:
        POP     SI                              ;Restore register values
        POP     BX
        POP     AX
        RET                   

Left     ENDP


; Right
;
; Description:          This function sends the commands necessary to move the
;                       robotrike right with MAXSPEED.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 90 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

Right           PROC        NEAR
                PUBLIC      Right 

RightInit:
        PUSH    AX                              ;Save old register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX for usage
        MOV     SI, OFFSET(array)               ;Get offset of array
       
RightClrAngle:
        MOV     AL, 'D'                         ;D is the command for angle
        MOV     [SI], AX                        ;Stoe command in array
        INC     SI
        MOV     AX, angle                       ;Get current angle and take its
        NEG     AX                              ;-ve value
        PUSH    SI
        CALL    Dec2String                      ;Convert value to ASCII
        POP     SI                              
        ADD     SI, 6                           ;The conversion occupies 6 spots
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET      
        INC     SI            

RightNewAngle:
        MOV     angle, 90                       ;Right is 90 degrees
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX
        INC     SI
        MOV     AX, angle                       
        PUSH    SI
        CALL    Dec2String                      ;Convert angle to ASCII char
        POP     SI      
        ADD     SI, 6                           ;ASCII char uses 6 spots
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI           

RightSpeed:
        MOV     speed, MAXSPEED                 ;New speed is MAXSPEED
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;S is the command for speed
        MOV     [SI], AX
        INC     SI      
        MOV     AX, speed                       
        PUSH    SI
        CALL    Dec2String                      ;Convert speed to ASCII char
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spots
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset and use it
                                                ;as counter
RightSerialLoop:
        CMP     BX, SI                          ;Grab each character in array
        JAE     RightDone                       ;and send over serial using
        MOV     AL, [BX]                        ;SerialPutChar
        CALL    SERIALPUTCHAR
        INC     BX
        JMP     RightSerialLoop

RightDone:
        POP     SI                              ;Restore old register value
        POP     BX
        POP     AX
        RET                   

Right     ENDP


; Stop
;
; Description:          This function sends the commands necessary to Stop the
;                       robotrike and reset the angles.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a 
;                       command for the trike to stop moving.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

Stop            PROC        NEAR
                PUBLIC      Stop
StopInit:
        PUSH    AX                              ;Save register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX
        MOV     SI, OFFSET(array)               ;Get offset for array

StopClrAngle:
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        PUSH    SI
        MOV     AX, angle                       ;Get current angle and take its
        NEG     AX                              ;-ve value
        CALL    Dec2String                      ;Convert Angle to ASCII char
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spots
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     angle, 0                    

StopSpeed:
        MOV     speed, 0                        ;New speed is 0
        XOR     AX, AX                          ;Clears AX for usage
        MOV     AL, 'S'                         ;S is the command for speed
        MOV     [SI], AX                        ;Store command in array
        INC     SI
        PUSH    SI
        MOV     AX, speed                       
        CALL    Dec2String                      ;Convert speed to ASCII char
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spots
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset of array
                                                ;to use as counter
StopSerialLoop:
        CMP     BX, SI                          ;Keep grabing each char in array
        JAE     StopDone                        ;and output to serial using
        MOV     AL, [BX]                        ;SerialPutChar
        CALL    SERIALPUTCHAR
        INC     BX
        JMP     StopSerialLoop

StopDone:       
        POP     SI                              ;Restore regiser values
        POP     BX
        POP     AX
        RET                   

Stop         ENDP


; Backward
;
; Description:          This function sends the commands necessary to move the
;                       robotrike Backward at MAXSPEED.
;
; Operation:            This function looks up the current angle and sends the 
;                       command to return the angle to 0.  It then sends a new
;                       angle of 180 degrees.  Then it sends a command for going
;                       forward at fullspeed.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, Speed, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

Backward        PROC        NEAR
                PUBLIC      Backward 

BackwardInit:
        PUSH    AX                              ;Save register values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears register for use
        MOV     SI, OFFSET(array)               ;Get offset for array
       
BackwardClrAngle:
        MOV     AL, 'D'                         ;D is the command for angle
        MOV     [SI], AX                        ;Store command in AX
        INC     SI
        MOV     AX, angle                       ;Get current angle and take its
        NEG     AX                              ;-ve value
        PUSH    SI
        CALL    Dec2String                      ;Convert value into ASCII
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spaces
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI            

BackwardNewAngle:       
        MOV     angle, 180                      ;New angle is 180 degrees
        XOR     AX, AX                          ;Clears AX for use
        MOV     AL, 'D'                         ;D is the command for angles
        MOV     [SI], AX
        INC     SI
        MOV     AX, angle       
        PUSH    SI
        CALL    Dec2String                      ;Convert angle to ASCII char
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spaces
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI           

BackwardSpeed:
        MOV     speed, MAXSPEED                 ;New speed is MAXSPEED
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;S is the command for Speed
        MOV     [SI], AX                        ;store command in array
        INC     SI      
        MOV     AX, speed                       
        PUSH    SI
        CALL    Dec2String                      ;Convert speed to ASCII char
        POP     SI
        ADD     SI, 6                           ;ASCII char occupies 6 spaces
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset and use as
                                                ;counter
BackwardSerialLoop:     
        CMP     BX, SI                          ;Grab each char in array and 
        JAE     BackwardDone                    ;output to serial using 
        MOV     AL, [BX]                        ;SerialPutChar
        CALL    SERIALPUTCHAR
        INC     BX
        JMP     BackwardSerialLoop

BackwardDone:
        POP     SI                              ;Restore register values
        POP     BX
        POP     AX
        RET                   

Backward     ENDP


; IncSpeed
;
; Description:          This function sends the commands necessary to increase
;                       the robotrike speed.  Each time this function is called,
;                       the speed increases by SPEEDADD.  The speed cannot be 
;                       more than MAXSPEED.
;
; Operation:            This funcion sends over command 'S' to change
;                       the speed by SPEEDADD using SerialPutChar.  The commands
;                       are initially saved in array before being sent over
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Speed, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/11/13

IncSpeed        PROC        NEAR
                PUBLIC      IncSpeed

IncSpeedInit:
        PUSH    AX                              ;Save regiser values
        PUSH    BX
        PUSH    SI
        MOV     SI, OFFSET(array)               ;Get the offset for array

IncSpeedAdd:
        ADD     speed, SPEEDADD                 ;Add to current speed
        CMP     speed, MAXSPEED                 ;If speed > MAXSPEED
        JBE     IncSpeedASCII                   ;If no, we're ok
        MOV     speed, MAXSPEED                 ;If yes, set to MAXSPEED
        JMP     IncSpeedASCII
        
IncSpeedASCII:        
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;S is the command for Abs[speed]
        MOV     [SI], AX                        ;Store command
        INC     SI      
        MOV     AX, speed
        PUSH    SI                              ;Converts the Speed into ASCII
        CALL    Dec2String                      ;using Dec2String
        POP     SI                              ;The ASCII values occupies 6 
        ADD     SI, 6                           ;spots in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Store initial offset of array
                                                ;in register to use as counter
IncSpeedSerialLoop:
        CMP     BX, SI                          ;Keep grabing characters from
        JAE     IncSpeedDone                    ;array until all commands are
        MOV     AL, [BX]                        ;sent over.  SerialPutChar is
        CALL    SERIALPUTCHAR                   ;used to send command over
        INC     BX
        JMP     IncSpeedSerialLoop

IncSpeedDone:
        POP     SI                              ;Restore register values
        POP     BX
        POP     AX
        RET                   

IncSpeed        ENDP

; DecSpeed
;
; Description:          This function sends the commands necessary to decrease
;                       the robotrike speed by SPEEDADD.  
;
; Operation:            This funcion sends over command 'S' to change
;                       the speed by SPEEDADD using SerialPutChar.  The commands
;                       are initially saved in array.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Speed, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

DecSpeed        PROC        NEAR
                PUBLIC      DecSpeed

DecSpeedInit:
        PUSH    AX                              ;Save regiser values
        PUSH    BX
        PUSH    SI
        MOV     AX, 0                           ;Clears AX
        MOV     SI, OFFSET(array)               ;Get the offset for array

DecSpeedAdd:
        SUB     speed, SPEEDADD                 ;Subtract from current speed
        CMP     speed, 0                        ;Check if speed < 0
        JGE     DecSpeedASCII                   ;If no, we're fine
        MOV     speed, 0                        ;Otherwise, set speed to 0
        JMP     DecSpeedASCII
        
DecSpeedASCII:        
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'S'                         ;S is the command for Abs[speed]
        MOV     [SI], AX                        ;Store command
        INC     SI      
        MOV     AX, speed                       ;Converts the Speed into ASCII
        PUSH    SI                              ;using Dec2String
        CALL    Dec2String
        POP     SI                              ;The ASCII values occupies 6 
        ADD     SI, 6                           ;spots in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Store initial offset of array
                                                ;in register to use as counter
DecSpeedSerialLoop:
        CMP     BX, SI                          ;Keep grabing characters from
        JAE     DecSpeedDone                    ;array until all commands are
        MOV     AL, [BX]                        ;sent over.  SerialPutChar is
        CALL    SERIALPUTCHAR                   ;used to send command over
        INC     BX
        JMP     DecSpeedSerialLoop

DecSpeedDone:
        POP     SI                              ;Restore old register values
        POP     BX
        POP     AX
        RET                   

DecSpeed        ENDP


; IncAngle
;
; Description:          This function sends the commands necessary to increase
;                       the robotrike angle by ANGLEADD.  
;
; Operation:            This funcion sends over command 'D' to change the angle
;                       by ANGLEADD using SerialPutChar.  The commands
;                       are initially saved in array.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

IncAngle        PROC        NEAR
                PUBLIC      IncAngle

IncAngleInit:
        PUSH    AX                              ;Save register values
        PUSH    BX
        PUSH    SI
        MOV     SI, OFFSET(array)               ;Get the offset for array
        ADD     angle, ANGLEADD                 ;Add to current angle
        
IncAngleASCII:        
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'D'                         ;D changes the relative angle
        MOV     [SI], AX                        ;Store Command 'D' in array
        INC     SI      
        MOV     AX, ANGLEADD                    ;A change of +ve of ANGLEADD
        PUSH    SI
        CALL    Dec2String                      ;Converts the angle to ASCII
        POP     SI                              ;Dec2String convertion occupies
        ADD     SI, 6                           ;6 spots in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset to use as
                                                ;counter
IncAngleSerialLoop:
        CMP     BX, SI                          ;Keep grabing characters from
        JAE     IncAngleDone                    ;array until all commands are
        MOV     AL, [BX]                        ;sent over.  SerialPutChar is
        CALL    SERIALPUTCHAR                   ;used to send command over
        INC     BX
        JMP     IncAngleSerialLoop

IncAngleDone:
        POP     SI                              ;Restore old register values
        POP     BX
        POP     AX
        RET                   

IncAngle        ENDP


; DecAngle
;
; Description:          This function sends the commands necessary to decrease
;                       the robotrike angle by ANGLEADD.  
;
; Operation:            This funcion sends over command 'D' to change
;                       the angle by ANGLEADD using SerialPutChar.  The commands
;                       are initially saved in array.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle, array
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

DecAngle        PROC        NEAR
                PUBLIC      DecAngle

DecAngleInit:
        PUSH    AX                              ;Save register values
        PUSH    BX
        PUSH    SI
        MOV     SI, OFFSET(array)               ;Get the offset for array
        SUB     angle, ANGLEADD                 ;Subtract to current angle
        
DecAngleASCII:        
        XOR     AX, AX                          ;Clears AX
        MOV     AL, 'D'                         ;D changes the relative angle
        MOV     [SI], AX                        ;Store Command 'D' in array
        INC     SI    
        MOV     AX, ANGLEADD                    
        NEG     AX                              ;A change of -ve of ANGLEADD
        PUSH    SI
        CALL    Dec2String                      ;Converts the angle to ASCII
        POP     SI                              ;Dec2String convertion occupies
        ADD     SI, 6                           ;6 spots in array
        XOR     AX, AX
        MOV     AL, CRET
        MOV     [SI], AX                        ;End the command with CRET
        INC     SI
        MOV     BX, OFFSET(array)               ;Get initial offset to use as
                                                ;counter
DecAngleSerialLoop:
        CMP     BX, SI                          ;Keep grabing characters from
        JAE     IncAngleDone                    ;array until all commands are
        MOV     AL, [BX]                        ;sent over.  SerialPutChar is
        CALL    SERIALPUTCHAR                   ;used to send command over 
        INC     BX
        JMP     IncAngleSerialLoop

DecAngleDone:
        POP     SI                              ;Restore old register values
        POP     BX
        POP     AX
        RET                   

DecAngle        ENDP


; LEDOn
;
; Description:          This function sends the commands necessary to turn on 
;                       the laser.  
;
; Operation:            Sends the command F over serial and store the LASERON
;                       in shared variable
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Laser
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/12/13

LEDOn           PROC        NEAR
                PUBLIC      LEDOn

        PUSH    AX                              ;Save old register value
        MOV     AL, 'F'                         ;Send command F over serial
        MOV     Laser, LASERON                  ;Store command locally
        CALL    SERIALPUTCHAR                   ;Send command over serial
        MOV     AL, CRET                        ;Send CRET over serial for      
        CALL    SERIALPUTCHAR                   ;end of command
        POP     AX                              ;Restore old register values
        RET                   

LEDOn   ENDP

; LEDOff
;
; Description:          This function sends the commands necessary to turn off 
;                       the laser.  
;
; Operation:            Sends the command O over serial to turn laser off.  
;                       Store LASEROFF in a shared variable.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Laser
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

LEDOff          PROC        NEAR
                PUBLIC      LEDOff

        PUSH    AX                              ;Save register values
        MOV     AL, 'O'                         ;O is the command for turning 
                                                ;laser off
        MOV     Laser, LASEROFF                 ;Laser is off
        CALL    SERIALPUTCHAR                   ;Send command over using 
                                                ;SerialPutChar 
        MOV     AL, CRET                        ;Send CRET to signify end of 
        CALL    SERIALPUTCHAR                   ;command
        POP     AX                              ;Restore register values
        RET                   

LEDOff  ENDP

; KeyNOP
;
; Description:          This function does nothing for the key.
;
; Operation:            None
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Speed
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

KeyNOP          PROC        NEAR
                PUBLIC      KeyNOP

        RET                   

KeyNOP         ENDP


; DisplaySpeed
;
; Description:          This function gets the current speed of the robotrike
;                       and display it on the LED blocks.  The display is
;                       left alligned.  
;
; Operation:            Gets the current speed from the shared variable and 
;                       calls DisplayNum.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Speed
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          LED only has 8 blocks.
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

DisplaySpeed    PROC        NEAR
                PUBLIC      DisplaySpeed

        PUSH    AX                              ;Save old register values
        MOV     AX, speed                       ;Get the speed
        CALL    DisplayNum                      
        POP     AX                              ;Restore register values
        RET                   

DisplaySpeed   ENDP


; DisplayAngle
;
; Description:          This function gets the current angle of the robotrike
;                       and display it on the LED block  
;
; Operation:            Gets the current angle from the shared variable calls 
;                       DisplayNum to display the angle on LED blocks.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       None
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          LED only has 8 blocks.
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

DisplayAngle    PROC        NEAR
                PUBLIC      DisplayAngle

        PUSH    AX                              ;Save old register values
        MOV     AX, angle                       ;Get the angle
        CALL    DisplayNum                      ;Display the angle
        POP     AX                              ;Restore register value
        RET                   

DisplayAngle   ENDP


; BufferFull
;
; Description:          This function is called when there is a problem with the
;                       buffer overflowing.  This function enqueues the error 
;                       event.
;
; Operation:            This function enqueues the error event of having a 
;                       filled buffer.
;
; Arguments:            None
;
; Return Value:         None
; Local Variables:      None
; Shared Variables:     Angle
;
; Global Variables:     None
; Input:                None
; Output:               None
; 
; Error Handling:       Do nothing.  The array will eventually have space.
;
; Algorithms:           None
;
; Data Structures:      Queues
;
; Limitations:          None
;
; Known Bugs:           None
;
; Registers Changed:    None
;       
; Author:               Sith Domrongkitchaiporn
; Last Modified:        12/17/13

BufferFull      PROC        NEAR
                PUBLIC      BufferFull

        PUSH    AX                              ;Save register values
        PUSH    SI
        MOV     AH, ERROREVENT                  ;Get event type
        MOV     AL, 0
        CALL    EnqueueEvent                    ;EnqueueEvent
        POP     SI                              ;Restore old register values
        POP     AX
        RET                   

BufferFull     ENDP


CODE    ENDS

; Data

DATA    SEGMENT PUBLIC  'DATA'

laser           DB    ?                         ;Laser On/Off
angle           DW    ?                         ;Angle of RoboTrike
speed           DW    ?                         ;Speed of RoboTrike
array           DB  (20) DUP (?)               ;Array for storing ASCII digits
display         DB  (8) DUP (?)                 ;Array for storing display

DATA ENDS

END
