;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                   Main Loop                                ;
;                                Main Functions                              ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Descriptions and ToC
;
; Revision History:
;       12/12/13   Sith Domrongkitchaiporn   Initial Revision
;       12/17/13   Sith Domrongkitchaiporn   Update Comments

MAXSPEED        EQU     32767                   ;Max allowed speed for Trike
SPEEDADD        EQU     5000                    ;Change of speed
ANGLEADD        EQU     5                       ;Change of angle
CRET            EQU     0dh                     ;Carriage Return        
ERROREVENT      EQU     0                       ;Error Event Type
LASERON         EQU     1                       
LASEROFF        EQU     0
