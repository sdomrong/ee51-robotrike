        NAME    MAIN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    MAIN                                    ;
;                            Homework #9 Test Code                           ;
;                                  EE/CS 51                                  ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Description:  This program tests the remote side of the RoboTrike function.
;               loop initializes the display, keypad, the serial, UI, the main
;               functionsand the event handlers.     
;
; Revision History:
;    12/14/13  Sith Domrongkitchaiporn	initial version
;    12/17/13  Sith Domrongkitchaiporn  Update Comments

CGROUP  GROUP   CODE
DGROUP  GROUP   STACK, DATA

CODE    SEGMENT PUBLIC 'CODE'


        ASSUME  CS:CGROUP, DS:DGROUP, SS:DGROUP

;external function declarations
        EXTRN   DisplayInit:NEAR
        EXTRN   InitCS:NEAR
        EXTRN   ClrIRQVectors:NEAR
        EXTRN   InstallHandler:NEAR
        EXTRN   InitTimer:NEAR
        EXTRN   KeypadInit:NEAR       
        EXTRN   InitINT2:NEAR
        EXTRN   SerialInit:NEAR
        EXTRN   DequeueEvent:NEAR
        EXTRN   MainInit:NEAR
        EXTRN   UIInit:NEAR
        
START:

MAIN:
        CLI                             ;Clear any initial interrupts
        MOV     AX, STACK               ;initialize the stack pointer
        MOV     SS, AX
        MOV     SP, OFFSET(TopOfStack)
        
        MOV     AX, DGROUP              ; initialize the data segment
        MOV     DS, AX
        
        CALL    DisplayInit             ;initialize display

        CALL    InitCS                  ;initialize the 80188 chip selects
                                        ;   assumes LCS and UCS already setup

        CALL    ClrIRQVectors           ;clear (initialize) interrupt vector table

        CALL    InstallHandler          ;install the event handler
                                        ;   ALWAYS install handlers before
                                        ;   allowing the hardware to interrupt.

        CALL    InitTimer               ;initialize the internal timer
        
        CALL    InitInt2                ;Initialize INT2 (for Serial)
        
        CALL    SerialInit              ;Initialize Serial

        
        CALL    KeypadInit              ;Initializes Keypad
        
        CALL    MainInit                ;Initialize the main functions
        
        CALL    UIInit                  ;Initializes the UI
        
        STI                             ;and finally allow interrupts.

Forever:
        CALL    DequeueEvent            ;Keep dequeuing events
        JMP     Forever                 ;sit in an infinite loop, nothing to
                                        ;   do in the background routine
        HLT                             ;never executed (hopefully)



CODE    ENDS

DATA    SEGMENT PUBLIC  'DATA'

DATA ENDS


; Stack
STACK   SEGMENT STACK  'STACK'

                DB      80 DUP ('Stack ')       ;240 words

TopOfStack      LABEL   WORD

STACK   ENDS

END     START   
