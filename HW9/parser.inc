;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                            ;
;                                    Parser                                  ;
;                               Parser Functions                             ;
;                                   EE/CS 51                                 ;
;                                                                            ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Include file for Parser

; Revision History:
;       12/11/13   Sith Domrongkitchaiporn      Initial Revision


;State Machine Constants
ST_INITIAL      EQU     0                       ;Initial State
ST_COMA         EQU     1                       ;Command with arguments state
ST_SIGN         EQU     2                       ;Sign State
ST_DIGIT        EQU     3                       ;Digit State
ST_COMS         EQU     4                       ;Speed command state
ST_COMN         EQU     5                       ;Comm with no arguments state

;Tokens Constats
TOKEN_COMA      EQU     0                       ;Commands with arguments
TOKEN_COMN      EQU     1                       ;Commands with no arguments
TOKEN_COMS      EQU     2                       ;Speed Command
TOKEN_POSSIGN   EQU     3                       ;+ve Sign
TOKEN_NEGSIGN   EQU     4                       ;-ve Sign
TOKEN_DIGIT     EQU     5                       ;Digits
TOKEN_RET       EQU     6                       ;Carriage Return
TOKEN_OTHER     EQU     7                       ;Unused characters
TOKEN_IGNORE    EQU     8                       ;Characters to ignore

NUM_TOKEN_TYPES EQU     9                       ;There are 9 token types

TOKEN_MASK      EQU     01111111B               ;mask high bit of token  
MAXSPEED        EQU     65534                   ;Max speed allowed
DONE            EQU     0                       ;Parsing correctly
RETURN          EQU     1                       ;Error Return
POSITIVE        EQU     0                       ;Positive Sign
NEGATIVE        EQU     1                       ;Negative Sign
LASERON         EQU     1                       ;Turn laser on
NOLASER         EQU     0                       ;Turn laser off
IGNORESPEED     EQU     65535                   ;Value for ignoring speed
IGNOREANGLE     EQU     -32768                  ;Value ofr ignoring angle
MAXANGLE        EQU     360                     ;Max angle for RoboTrike

BUFFER          EQU     20                      ;Buffer length
ABSOLUTE        EQU     2                       ;Absolute value sign
MAXELEV         EQU     60                      ;Max elevation for turret
MINELEV         EQU     -60                     ;Min elevation for turrte

;Token Values
COMMANDS        EQU     0                       ;Speed Command
COMMANDV        EQU     1                       ;Relative Speed Command
COMMANDD        EQU     2                       ;Direction Command
COMMANDT        EQU     3                       ;Turret Command
COMMANDE        EQU     4                       ;Elevation Command
COMMANDF        EQU     5                       ;Fire Laser Command
COMMANDO        EQU     6                       ;Laser OFF Command
